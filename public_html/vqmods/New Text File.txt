<modification>
    <id>Show subcategory products on parent category</id>
    <version>1.0</version>
    <vqmver>2.4.1</vqmver>
    <author>cobisimo@gmail.com</author>

    <file name="catalog/controller/product/category.php" error="skip">
        <operation error="skip">
            <search position="after">
                'filter_category_id' => $category_id,
            </search>
            <add>
                'filter_sub_category' => true,
            </add>
        </operation>
    </file>
</modification>