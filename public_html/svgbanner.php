<?php 
  $title = 'SVG Logo';
  $jb    = 'by: John_Betong';
  $sp    = 'https://www.sitepoint.com/community/t/'
         . 'using-svg-format-for-responsive-website-banner/232555';
  $img   = 'leaves-bright.jpg';       
  $svg   = '<svg width="100%" height="auto" viewBox="0 0 648 400">'
         . ' <image'
         .'     xlink:href="' .$img .'"'
         .'     x="0" y="0"' 
         .'     width="648px" height="400px"'
         .'  />'
         .'</svg>'
         ;

?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1">
<title> <?= $title ?> </title>
<link rel="stylesheet" href="screen.css" media="screen">
<style media="screen">
body {background: #f0f0f0;}
#logo{width:100%; height:auto;}
#logo div {width:200px; height:auto;}
.bg1 {background-color:cyan;} .bgs {background-color: snow;}
.clb {clear:both;}
.fll {float:left;} .flr {float:right;}
.mga {margin:auto;}
.ooo {border:0; margin:0; padding:0;}
.p42 {padding:0.42em;}
.w88 {width:88%;}
.tac {text-align:center;}
</style>
</head>
<body> 

  <div id="logo" class="fll bg1 tac">
    <a class="flr" href="<?= $sp ?>"> SitePoint Forum </a>
    <div class="fll">
      <?= $svg ?>
    </div>    
    <h1> <?= $title ?> </h1>
    <h5> <?= $jb ?> </h5>
  </div>

  <p class="w88 mga p42 bgs"> <?php // highlight_file(__FILE__) ?> </p>
</body>
</html>