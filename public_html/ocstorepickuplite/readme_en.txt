=== OC-Store: Pickup lite ===
Developer: Sergey Pechenyuk 
Site: http://oc-store.com
Tags: shipping, pickup
Product version: 1.0
OpenCart verify version: 2.x
License: GPLv2 or later

== Description ==
Module for creating a list of shipment points for ordering.
This module will let you to specify the parameters of shipment points: the name of the point, its location (city and address), working time, email and shipping cost to each point.
The module "OC-Store: Pickup light" is set as the delivery module and works with all popular themplates.

== Install ==
How to install the module?
1. Download ocstore_pickup_lite_1_0_for_opencart_2_x.ocmod.zip through Extension Installer.
2. In the Modifications click "Refresh" button to rebuild the cache.
3. In the Modules install the module "OC-Store: Pickup lite".
4. Configure the module "OC-Store: Pickup lite" as you need.
5. Start to use it free of charge.

== Version ==
26.01.2016: First version of module "OC-Store: Pickup lite".




