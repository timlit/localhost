=== OC-Store: Pickup lite ===
Developer: Sergey Pechenyuk 
Site: http://oc-store.com/pickup_lite
Tags: ��������, ���������
Product version: 1.0
OpenCart verify version: 2.x
License: GPLv2 or later

== �������� ==
�������� ������ ������� ���������� ��� ���������� ������. 

== ��������� ==
1. ��������� ���� ocstore_pickup_lite_1_0_for_opencart_2_x.ocmod.zip ����� ����������� �������� ��������� ����������.
2. � ��������� ������� �������� ���������� ��������� ������ "OC-Store: Pickup lite". 
3. ��������� ������ �������� "OC-Store: Pickup lite".
4. ������ ������������.

== ������ ==
31.01.2016: ������ ������ ������ "OC-Store: Pickup lite".
