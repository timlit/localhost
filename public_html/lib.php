<?php

//** FUNCTIONS **//

/*
CREATE FIELDS:
code
name
*/
function csvbulk($file, $seperator){
$csvbulk = array();

if (file_exists($file)){

  $fitems = fopen($file,"r");
  while(! feof($fitems))
  { $i += 1;

  $csvbulk[$i] = (fgetcsv($fitems,0,$seperator));

  }//while
  fclose($fitems);
}//if file_exists


return $csvbulk;
}//csvbulk


function oc_products($dbname, $clmatrix, $total, $leadclm, $totalrecids, $categoryyes = 0){
// return $total;
$updateInterval = 5;
$cr = "\n\r"; //$cr = '';
$crshort = "\n"; ;
// manufacturer_id, 
if($categoryyes == 1) {
	$catclm = "category_id,";
}else{
	$catvalueoff = " -- ";
}
/*
manufacturer_id, 
image,
 code, 
*/

foreach ($clmatrix as $clm =>$cv){
 if($cv['active']=='1'){
$clm_insert .= ', '.$clm;
$clm_update .= ', '.$clm.' = VALUES('.$clm.')'.$crshort;
}
}

$sqlstart = "INSERT INTO $dbname.oc_product  (product_id
--
$catclm
$clm_insert
, status, 
-- ,stock_status_id
, sort_order,  date_added) VALUES 
"; 

// $sqlstart = "UPDATE $dbname.oc_product  
// set (product_id, model, code, name, quantity, price, status, sort_order, date_available, date_added) VALUES 
// "; 

$sqlstart2 = "
UPDATE $dbname.oc_product_description d
LEFT JOIN $dbname.oc_product p
ON p.product_id = d.product_id

SET 

";


/*
-- set @date = date_format(now(), '%Y-%m-%d');
-- set @date = '2017-04-17';
-- WHERE date_added BETWEEN concat( @date, ' 00:00:00' ) AND concat( @date, ' 23:59:59' )

*/

$onduplicateclause = "

ON DUPLICATE KEY UPDATE
product_id = VALUES(product_id)
-- , manufacturer_id = VALUES(manufacturer_id)
-- ,  code = VALUES(code)
$clm_update
/*
, name = VALUES(name)
, model = VALUES(model)
, sku = VALUES(sku)
, quantity = VALUES(quantity)
, price = VALUES(price)
*/
-- , image = VALUES(image)
-- , category_id = VALUES(category_id)


, status = (1)
, sort_order = 100
, date_modified = (CURRENT_TIMESTAMP ())

;

";


$sqlend = $onduplicateclause."

-- 
insert into $dbname.oc_product_description (product_id, code, language_id, name )
select distinct product_id, code, 1, name from $dbname.oc_product 
WHERE date_added BETWEEN (now() - interval $updateInterval minute) AND (now() + interval 1 minute)
OR date_modified BETWEEN (now() - interval $updateInterval minute) AND (now() + interval 1 minute)

ON DUPLICATE KEY UPDATE
product_id = VALUES(product_id)
-- , code = VALUES(code)
, language_id = VALUES(language_id)
, name = VALUES(name)
;


insert into $dbname.oc_product_to_store (product_id, store_id )
select distinct product_id, 0 from $dbname.oc_product 
WHERE date_added BETWEEN (now() - interval $updateInterval minute) AND (now() + interval 1 minute)
OR date_modified BETWEEN (now() - interval $updateInterval minute) AND (now() + interval 1 minute)

ON DUPLICATE KEY UPDATE
product_id = VALUES(product_id)
, store_id = VALUES(store_id)
;

/**/
insert into $dbname.oc_product_to_category (product_id, category_id, main_category )
select distinct product_id, category_id, 1 from $dbname.oc_product 
WHERE date_added BETWEEN (now() - interval $updateInterval minute) AND (now() + interval 1 minute)
OR date_modified BETWEEN (now() - interval $updateInterval minute) AND (now() + interval 1 minute)

ON DUPLICATE KEY UPDATE
product_id = VALUES(product_id)
-- 
, oc_product_to_category.category_id = IF (main_category = 1, VALUES(category_id),  (oc_product_to_category.category_id )) /* other cats set manually */
-- , main_category = VALUES(main_category)
;



";

$sqlbulk = '';
$result = '';
$linelimit  = "; @#line". "\n\r";
$lineconcat = ", ";
$i= 0; $recids = array();
$notfound = 0; $updated = 0; $newpricecount = 0; $all_lead = '';
foreach ($total as $kkey => $vvalue){
 
  if ($i==101) { $i = 1; $lineswitch = $onduplicateclause.$sqlstart. "\n\r"; } else { $i += 1; $lineswitch = $lineconcat; }
	  // $result =  ($vvalue[3]);
  // if($vvalue[1] == 'Штрих-код' || $vvalue[5] == '' ) continue;

    // print_r($clmatrix);
    // print_r($totalrecids);
    // print_r($vvalue);
  
  $code = $vvalue[0];
    // print_r($totalrecids[$code]['pid']);exit;
  // $code = $vvalue[2]; echo $code."\n\r"; continue;
// if(!strpos($totalrecids[$code][$leadclm],';')) {$ta_lead  = $totalrecids[$code][$leadclm] . ';'; }else{ $ta_lead  = $totalrecids[$code][$leadclm]; }
if(!strpos($totalrecids[$code]['pid'],';')) {$ta_lead  = $totalrecids[$code]['pid'] . ';'; }else{ $ta_lead  = $totalrecids[$code][$leadclm]; }
if(!strpos($totalrecids[$code]['price'],';')) {$ta_price  = $totalrecids[$code]['price'] . ';'; }else{ $ta_price  = $totalrecids[$code]['price']; }
$all_lead .= $ta_price."\n\r";
  $recids = explode(';', $ta_lead  ); //
  $prices = explode(';', $ta_price ); //
if ($ta_lead=='') {$notfound += 1;}else{$barcodes +=1;}

  //  $photo = $vvalue[8]; 
  
// if ($price == '' || $price == 0) continue;
  $rci = -1; 
foreach ($recids as $rkey => $rvalue){
	// print_r($rvalue.$crshort);
	 $rci += 1;
	// if($rvalue == '' || $prices[$rci]='') continue;//empty array element
	// if($price.'00'<>$prices[$rci].'' ) $newpricecount +=1;
	$updated += 1;
	if(strpos($prices[$rci],$price)<1 ) $newpricecount +=1;


	// $thisrec = $rvalue;
	// if($rvalue=='') continue;
	// '$manufacturer', 
	/*
	'%m/%d/%Y'
	'$manufacturer', 
	'$photo',
	'$code',
	*/

	  // $sqlbulk .= " ('$rvalue', '$model',  '$model',   '$title', '$quantity', $price, 1, 100, STR_TO_DATE('$date_available', '%d.%m.%Y'), CURRENT_TIMESTAMP(), STR_TO_DATE('$expiration', '%d.%m.%Y') )".$crshort.$lineswitch; //.$cr
	  $sqlbulk .= ",(". $rvalue.",". "";
	  $mi = 0;
	  	  foreach ($clmatrix as $clm =>$cv){
	
		  if($cv['active']=='1'){
			  // $sqlbulk .= $crshort.'/*'.$clm.' - '.$cv['column'].'*/';
		  if($cv['type']=='date'){
		  $sqlbulk .= "STR_TO_DATE('".$vvalue[$mi]."', '%d.%m.%Y'),";
		  }else{
		  $sqlbulk .= "'".$vvalue[$mi]."',";
		  }
		  		  // $sqlbulk .= "'".$vvalue[$mi]."',";
		  
		  }
		  $mi +=1;
}
 $sqlbulk .= " 1, 100, CURRENT_TIMESTAMP() )".$crshort;
	}	
	
	  $sqlbulk .=  ",)".$crshort.$lineswitch; //.$cr

//[product_id], language_id, name, description $prices[$rci].
	
	  
  }
  
  
unset($total['Код товара']);
unset($total['Название товара']);
unset($total['Название товара_Фирма - производитель']);
unset($total['ИТОГО']);
unset($total['ИТОГО_']);

$sqlbulk = str_replace(",\n\r@#", "", $sqlbulk.'@#');


$sqlbulk = str_replace("STR_TO_DATE('$"."date_available', '%m/%d/%Y'),", "", $sqlbulk.'@#');


$sqlbulk = str_replace(",)", ")", $sqlbulk.'');
$sqlbulk = str_replace(", ,", ",", $sqlbulk.'');


$result = $sqlstart.$sqlbulk.$sqlend;
if(!strpos($sqlbulk, "STR_TO_DATE('$date_available',") ){
$result = str_replace("date_available, ", "", $result.'@#');
}
$result = str_replace(",\n\rINSERT", "\n\r;\n\rINSERT", $result.'@#');
$result = str_replace(",\n\r, @#", "\n\r;\n\r", $result.'');

$result = str_replace("\n\rON DUPLICATE", "ON DUPLICATE", $result);
$result = str_replace(", @#", "", $result.'@#');
$result = str_replace("@#", "", $result.'@#');

$ratio = $updated/$barcodes;
// print($all_lead);
return " UPDATE oc_product as p set quantity = 0; /* INSERT INTO u0193535_oc.oc_product_update_log (new, records, barcodes) VALUES ( $notfound, $updated, $barcodes );  newpricecount: $newpricecount, ratio: $ratio) */ \n\r".$result;
  // $result =  ($total);
  // echo count($total);
	
}//oc_products

function groupconcat_flat(array $array, $srcpar, $newpar){
/*
GROUP_CONCAT(   CONCAT(p.product_id, '-', p.price) SEPARATOR ';')

200117092002	О. Б. тампоны, №16 Прокомфорт Мини	65594-195.0000
200117090002	НАТРИЯ ХЛОРИД 0,9% 400мл р-р д/инфузий фл. Биохимик	42375-43.5000;63024-43.5000

    [200117092002] => Array
        (
            [name] => 
            [model] => 200117092002
            [pid] => 65594-195.0000
        )

    [200117090002] => Array
        (
            [name] => 
            [model] => 200117090002
            [pid] => 42375-43.5000;63024-43.5000
        )
*/
foreach ($array as $key => $value){
if(strpos($value['pid'], ';')){

}
}

return $array;
}

function oc_products2($dbname, $total, $totalrecids){
/*

*/
// return $total;
$cr = "\n\r"; //$cr = '';
$sqlstart = "INSERT INTO $dbname.oc_product  (product_id, manufacturer_id, model, code, name, quantity, price, status, sort_order, date_available, date_added) VALUES 
"; 

// $sqlstart = "UPDATE $dbname.oc_product  
// set (product_id, model, code, name, quantity, price, status, sort_order, date_available, date_added) VALUES 
// "; 

$sqlstart2 = "
UPDATE $dbname.oc_product_description d
LEFT JOIN $dbname.oc_product p
ON p.product_id = d.product_id

SET 

";


/*
-- set @date = date_format(now(), '%Y-%m-%d');
-- set @date = '2017-04-17';
-- WHERE date_added BETWEEN concat( @date, ' 00:00:00' ) AND concat( @date, ' 23:59:59' )

*/

$onduplicateclause = "

ON DUPLICATE KEY UPDATE
product_id = VALUES(product_id)
, manufacturer_id = VALUES(manufacturer_id)
,  name = VALUES(name)
,  code = VALUES(code)
, model = VALUES(model)
, quantity = VALUES(quantity)
, price = VALUES(price)
, date_available = VALUES(date_available)
, status = (1)
, sort_order = 100
, date_modified = (CURRENT_TIMESTAMP ())

;

";


$sqlend = $onduplicateclause."

-- 
insert into u0193535_oc.oc_product_description (product_id, code, language_id, name )
select distinct product_id, code, 1, name from u0193535_oc.oc_product 
WHERE date_added BETWEEN (now() - interval 15 minute) AND (now() + interval 1 minute)
OR date_modified BETWEEN (now() - interval 15 minute) AND (now() + interval 1 minute)

ON DUPLICATE KEY UPDATE
product_id = VALUES(product_id)
, code = VALUES(code)
, language_id = VALUES(language_id)
, name = VALUES(name)
;


insert into u0193535_oc.oc_product_to_store (product_id, store_id )
select distinct product_id, 0 from u0193535_oc.oc_product 
WHERE date_added BETWEEN (now() - interval 15 minute) AND (now() + interval 1 minute)
OR date_modified BETWEEN (now() - interval 15 minute) AND (now() + interval 1 minute)

ON DUPLICATE KEY UPDATE
product_id = VALUES(product_id)
, store_id = VALUES(store_id)
;

/**/
insert into u0193535_oc.oc_product_to_category (product_id, category_id, main_category )
select distinct product_id, 0, 0 from u0193535_oc.oc_product 
WHERE date_added BETWEEN (now() - interval 15 minute) AND (now() + interval 1 minute)
OR date_modified BETWEEN (now() - interval 15 minute) AND (now() + interval 1 minute)

ON DUPLICATE KEY UPDATE
product_id = VALUES(product_id)
, category_id = VALUES(category_id)
, main_category = VALUES(main_category)
;



";

$sqlbulk = '';
$result = '';
$linelimit  = "; @#line". "\n\r";
$lineconcat = ", ";
$i= 0; $recids = array();
foreach ($total as $kkey => $vvalue){
 
  if ($i==101) { $i = 1; $lineswitch = $onduplicateclause.$sqlstart. "\n\r"; } else { $i += 1; $lineswitch = $lineconcat; }
	  // $result =  ($vvalue[3]);
  if($vvalue[1] == 'Штрих-код' || $vvalue[5] == '' ) continue;
  $model = $vvalue[1];
  $code = $kkey;
  // $recids = explode(';', $totalrecids[$code] );

  $manufacturer = $vvalue[2];
  
  // $manufacturer = $vvalue[2];
  $title = ($vvalue[3]);
  $quantity = $vvalue[4];   
  $price = $vvalue[5];  
  
  $date_available = $vvalue[6];   
  /**/
foreach ($recids as $rkey => $rvalue){
	// $thisrec = $rvalue;
	// if($rvalue=='') continue;
	  $sqlbulk .= " ('$rvalue', '$manufacturer', '$model', '$code', '$title', '$quantity', '$price', 1, 100, STR_TO_DATE('$date_available', '%d.%m.%Y'), CURRENT_TIMESTAMP() )".$cr.$lineswitch;
}
//[product_id], language_id, name, description
	  // $recids = ($totalrecids[$title] );
	  // $sqlbulk .= " ('$recids', '$manufacturer', '$model', '$code', '$title', '$quantity', '$price', 1, 100, STR_TO_DATE('$date_available', '%d.%m.%Y'), CURRENT_TIMESTAMP() )".$cr.$lineswitch;
	
	  
  }
  
  
unset($total['Код товара']);
unset($total['Название товара']);
unset($total['Название товара_Фирма - производитель']);
unset($total['ИТОГО']);
unset($total['ИТОГО_']);

$sqlbulk = str_replace(",\n\r@#", "", $sqlbulk.'@#');
$result = $sqlstart.$sqlbulk.$sqlend;

$result = str_replace(",\n\rINSERT", "\n\r;\n\rINSERT", $result.'@#');
$result = str_replace(",\n\r, @#", "\n\r;\n\r", $result.'');

$result = str_replace("\n\rON DUPLICATE", "ON DUPLICATE", $result);
$result = str_replace(", @#", "", $result.'@#');
$result = str_replace("@#", "", $result.'@#');

return $result;
  // $result =  ($total);
  // echo count($total);
	
}//sql_manufacturer


function oc_product_to_category($dbname, $bulk){
/*
20002704 -> 57
115161 -> 57
20003419 -> 57

*/
	$sqlstart = "INSERT INTO $dbname.oc_product_to_category (name) VALUES 
	";
	$sqlend = '';
	
	foreach ($bulk as $key => $value){
		if ($key == '') continue;
		$sqlbulk  .= "('$key'),";
		
	}
$sqlbulk = str_replace(",@#", "", $sqlbulk.'@#');
$sqlbulk = $sqlstart.$sqlbulk.$sqlend;	
	
	return $sqlbulk;
}//oc_product_to_category


function oc_manufacturer($dbname, $bulk){
	/*opencart manufacturer

	*/
	// echo count ( $bulk);

	$sqlstart = "INSERT INTO $dbname.oc_manufacturer (name, date_added) VALUES " . "\n\r";
	$sqlend = ";\n\r "."
INSERT INTO $dbname.oc_manufacturer_description (manufacturer_id, language_id, name)  
SELECT manufacturer_id, 1, name FROM oc_manufacturer
WHERE date_added BETWEEN (now() - interval 2 minute) AND (now() + interval 1 minute)
-- OR date_modified BETWEEN (now() - interval 15 minute) AND (now() + interval 1 minute)

";

	foreach ($bulk as $key => $value){
		if ($key == '') continue;
		$sqlbulk  .= "('$key', now() ),"."\n\r";
		
	}
$sqlbulk = str_replace(",\n\r@#", "\n\r", $sqlbulk.'@#');
$sqlbulk = $sqlstart.$sqlbulk.$sqlend;	
	
	return $sqlbulk;
}//oc_manufacturer


// function 

function print_bulk($bulk){
	
foreach ($bulk as $key => $value){
	$sql .= $value."'".$cr."\n\r". " or d.name =  '";
}


echo $sql;
	
}

function dbread(array $dbreq, $sql, $leadclm, array $fieldvalue){// $servername, $username, $password, $dbname, $sql

$conn = dbopen($dbreq);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


$result = array();
mysqli_set_charset($conn,"utf8");
// mysqli_set_charset($conn,"cp1251");
// mysqli_set_charset($conn,"latin1");
// mysqli_set_charset($conn,"ansi");

// $cl = $conn->query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'; ");
// $encodingprep = " SET character_set_results = 'utf8'; character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'; ";
$encodingprep ='';
$dbresult = $conn->query($encodingprep. $sql) 
or die(mysqli_error($conn))
;

// http://php.net/manual/en/mysqli-stmt.bind-result.php#92505
// call_user_func_array(array($mysqli_stmt_object, "bind_result"), $byref_array_for_fields);

// $results = array();
// while ($mysqli_stmt_object->fetch()) {
    // $results[] = $byref_array_for_fields;
	

// }
if ($dbresult->num_rows > 0) {

if(!$leadclm == '' ){
    while($row = $dbresult->fetch_assoc()) {
        // echo "SELECT manufacturer_id: " . $row["manufacturer_id"]. " - Name: " . $row["name"]. " " . $row["name"]. "<br>";
        // $result[(trim($row["name"]))]  = $row["manufacturer_id"];
		foreach ($fieldvalue as $key=>$value){
			// $result[(trim($key))]  = $row[$value];
			$result[$row[$leadclm]][(trim($value))]  = $row[$value];
		}
    }
	
}else{
	while($row = $dbresult->fetch_assoc()) {
        // $res.= "id: " . $row["groupid"]. "\n\r"; 
       array_push ($result, $row); //. "\n\r"
    }


}//if!$leadclm  == ''
	
} else {
     $result['note'] = "0 results";
}

$conn->close();
return ($result);
	
}//dbread


function product_codes($dbreq){
$categoryres = array();
$categoryres = db_read_categories($dbreq);
// $result = ($categoryres); exit;

$seourlres = array();
$seourlres = db_read_product_seourl($dbreq);
// $result = ($seourlres); exit;

$directres = array();
$directres = (db_read_direct()); 
// $result =  ($directres); exit;

/*
SET @myArrayOfValue = '2,5,2,23,6,';

WHILE (LOCATE(',', @myArrayOfValue) > 0)
DO
    SET @value = ELT(1, @myArrayOfValue);
    SET @myArrayOfValue= SUBSTRING(@myArrayOfValue, LOCATE(',',@myArrayOfValue) + 1);

    INSERT INTO `EXEMPLE` VALUES(@value, 'hello');
END WHILE;

//$seourlres
 [5nok-tbl-po-50mg-50] => Array
        (
            [code] => 2
            [product_id] => 34144
            [keyword] => 5nok-tbl-po-50mg-50
        )
    [173330] => Array
        (
            [code] => 173330
            [product_id] => 34147
            [keyword] => 9-mesyacev-omegamma-kaps-30
        )
		
		
*/


$pcodeid = array();

$thisurlarr = array(); $thisurl = '';
foreach($directres as $key => $value){
	
	$url = $value['url'];
	
	foreach ($seourlres as $skey => $svalue){
		$url = str_replace(''.$skey,'product_id='.$svalue['product_id'],$url); //break;
		
	}
	//63110 zoladeks-kaps-dlya-pk-vved-prolong-dejstv-36mg-shpric-1
// echo $url. ' -- product_id='.$svalue['product_id']; exit;
	
	
	$url = str_replace("//",'', $url);
	$url = str_replace('/','&', $url);
	$url = str_replace('=','&', $url);
	$url = str_replace('?','&', $url);
	
	foreach($categoryres as $ckey => $cvalue){
		$url = str_replace($cvalue."&",'',$url);
	}
	
	// $directres[$key]['url'] = $url;

	$thisurlarr = explode("&", $url);
	
	// $findinarr = array_search("product_id", $thisurlarr);
	// $result = ($thisurlarr); 	$result = ($findinarr);
	// if(strpos($thisurlarr[1], 'product_id=') > -1) {

	if(array_search("product_id", $thisurlarr) !== false){
		 $findinarr = array_search("product_id", $thisurlarr);
	$thisurl = $thisurlarr[$findinarr+1+1];
	
	// $arsize = (sizeof($thisurlarr)-1);


	// $thisurl = str_replace('product_id=', '', $thisurl);
	// $directres[$key]['id'] = ($thisurl);	
	// $directres[$key]['code'] = ($seourlres[$thisurl]['code']);	
	
	$pcodeid[$thisurl]['id'] = ($thisurl);
	$pcodeid[$thisurl]['code'] = ($seourlres[$thisurl]['code']);
	$pcodeid[$thisurl]['name'] = $seourlres[$thisurl]['name'];
	// $pcodeid[$seourlres[$thisurl]['code']] = ($thisurl);
	
	
	// }elseif(strpos($thisurlarr[2], 'product_id=') > -1) {
		// $thisurl = $thisurlarr[2];

	// }elseif(array_search("tbl", $thisurlarr) !== false){
	}else{
		
		// unset($directres[$key]);
		// continue;
	}
	

	
}
// $result =  ($directres); exit;

unset($pcodeid['search']);
// echo sizeof ($pcodeid). "\n\r";
return ($pcodeid);
}//product_codes



function db_read_categories(array $dbreq){

// $servername = "vip9.hosting.reg.ru";
// $username = "u0193535_newoc";
// $password = "wEnA2OLh";
// $dbname = "u0193535_oc";
//``````````````````````````````````
// Create connection
// ALTER TABLE `u0193535_oc`.`direct_apteka_sale` 
// CHANGE COLUMN `ID группы` `groupid` BIGINT(20) NULL DEFAULT NULL ;

$res = array();
$conn = new mysqli($dbreq['servername'], $dbreq['username'], $dbreq['password'], $dbreq['dbname']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
//`ID группы` as
$sql = "SELECT keyword FROM u0193535_oc.oc_url_alias
left join u0193535_oc.oc_category_description cd on replace(query, 'category_id=' , '') = cd.category_id
where query like '%category_id=%' 
-- and keyword like '%neiro%'
/*
and query not like '%manufacturer%'
and query not like '%product_id%'
and query not like '%information_id%'
and query not like '%product_id%'
and query not like '%news_id%'

and query not like '%account%'
*/
order by cd.category_id asc

-- LIMIT 1000 
; ";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
	   $res[$row['keyword']] = $row['keyword'];
    }
} else {
    echo "0 results";
}
$conn->close();
	
	// echo $res;
return $res;
	
}//db_read_categories

function db_read_direct(){
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "u0193535_oc";
//``````````````````````````````````
// Create connection
// ALTER TABLE `u0193535_oc`.`direct_apteka_sale` 
// CHANGE COLUMN `ID группы` `groupid` BIGINT(20) NULL DEFAULT NULL ;

$res = array();
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
mysqli_set_charset($conn,"utf8");
// mysqli_set_charset($conn,"cp1251");
//`ID группы` as
$sql = "SELECT  groupid, id_ad, url, phrase_minus  FROM u0193535_oc.direct_apteka_sale 
-- where not url like '%product_id%' and not url like '%tbl%'
-- SUBSTRING_INDEX(url, '?utm_source', 1)  --  as http://stackoverflow.com/questions/14950466/how-to-split-the-name-string-in-mysql
-- CHAR_LENGTH(url) as urlcount : first big, then small

where not url like '%/?%'
and not url like '%our-pharmacy%'
and not url like '%?search=%'

and phrase_minus like '%золадекс%'

-- ORDER BY urlcount desc
-- ORDER BY id_ad desc 
-- LIMIT 1000 
; ";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        // $res.= "id: " . $row["groupid"]. "\n\r"; 
       array_push ($res, $row); //. "\n\r"
    }
} else {
    echo "0 results";
}
$conn->close();
	
	// echo $res;
return $res;
	
}//db_read_direct


function db_read_product_seourl(){
$servername = "vip9.hosting.reg.ru";
$username = "u0193535_newoc";
$password = "wEnA2OLh";
$dbname = "u0193535_oc";
//``````````````````````````````````
// Create connection
// ALTER TABLE `u0193535_oc`.`direct_apteka_sale` 
// CHANGE COLUMN `ID группы` `groupid` BIGINT(20) NULL DEFAULT NULL ;

$res = array();
$conn = new mysqli($servername, $username, $password, $dbname);
mysqli_set_charset($conn,"cp1251");
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
//`ID группы` as
/*
SELECT query, keyword, p.product_id, date_added, p.name, code FROM u0193535_oc.oc_url_alias q
left join u0193535_oc.oc_product p
on replace(query, 'product_id=', '') = p.product_id
where query like 'product_id=%'
*/

$sql = "SELECT p.code, p.product_id, p.code, d.name, u.keyword, CHAR_LENGTH(keyword) as urlcount FROM u0193535_oc.oc_url_alias u
-- CHAR_LENGTH(keyword) as urlcount : first big, then small
left join u0193535_oc.oc_product p
on replace(query, 'product_id=', '') = p.product_id

left join u0193535_oc.oc_product_description d
on replace(query, 'product_id=', '') = d.product_id

where query like 'product_id=%' 
-- and keyword = '5nok-tbl-po-50mg-50'
ORDER BY urlcount desc
-- LIMIT 1000 
; ";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        // $res.= "id: " . $row["groupid"]. "\n\r"; 
       // array_push ($res['keyword'], $row); //. "\n\r"
        $res[$row['keyword'] ] = $row; //. "\n\r"
        $res[$row['product_id'] ] = $row; //. "\n\r"
    }
} else {
    echo "0 results";
}
$conn->close();
	
	// echo $res;
return $res;
	
}//



function dbopen(array $dbreq){// $servername, $username, $password, $dbname, $sql
// $servername = "localhost";
// $username = "root";
// $password = "";
// $dbname = "u0193535_oc";

// $servername = "vip9.hosting.reg.ru";
// $username = "u0193535_default";
// $password = "wpAPTEKA11";
// $dbname = "u0193535_oc";
//"set names utf8 collate utf8_general_ci ; set character set utf8;".

$conn = new mysqli($dbreq['servername'], $dbreq['username'], $dbreq['password'], $dbreq['dbname']);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
return $conn;


	
}//dbopen

function dbread2(){// $servername, $username, $password, $dbname, $sql
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "u0193535_oc";

$servername = "vip9.hosting.reg.ru";
$username = "u0193535_default";
$password = "wpAPTEKA11";
$dbname = "u0193535_oc";
//"set names utf8 collate utf8_general_ci ; set character set utf8;".
$sql =  " SELECT manufacturer_id, 1, name FROM oc_manufacturer ;";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 




$result = array();
mysqli_set_charset($conn,"utf8");
$dbresult = $conn->query($sql);


if ($dbresult->num_rows > 0) {
    // output data of each row
    while($row = $dbresult->fetch_assoc()) {
        // echo "SELECT manufacturer_id: " . $row["manufacturer_id"]. " - Name: " . $row["name"]. " " . $row["name"]. "<br>";
        $result[$row["manufacturer_id"]]  = trim($row["name"]);
    }
} else {
    echo "0 results";
}

$conn->close();
return ($result);
	
}//dbread2


function dbread3(){// $servername, $username, $password, $dbname, $sql
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "u0193535_oc";

$servername = "vip9.hosting.reg.ru";
$username = "u0193535_default";
$password = "wpAPTEKA11";
$dbname = "u0193535_oc";
//"set names utf8 collate utf8_general_ci ; set character set utf8;".
$sql =  " SELECT manufacturer_id, 1, name FROM oc_manufacturer ;";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


$result = array();
mysqli_set_charset($conn,"utf8");
$dbresult = $conn->query($sql);

$i = 0;
if ($dbresult->num_rows > 0) {
    // output data of each row
    while($row = $dbresult->fetch_assoc()) {
		$i += 1;
        // echo "SELECT manufacturer_id: " . $row["manufacturer_id"]. " - Name: " . $row["name"]. " " . $row["name"]. "<br>";
        // $result[$row["manufacturer_id"]]  = trim($row["name"]);
        $result[$i][0] = $row["manufacturer_id"] ;
        $result[$i][1] = trim($row["name"]) ;
    }
} else {
    echo "0 results";
}

$conn->close();
return ($result);
	
}//dbread3




function cleandb(){
/*
delete FROM u0193535_oc.oc_product
where product_id > 64388
;
delete FROM u0193535_oc.oc_product_description
where product_id > 64388
;
delete FROM oc_product_to_store
where product_id > 64388
;
delete FROM oc_product_to_category
where product_id > 64388
;

*/

}

function properCaseSmart($string){ //return (strtolower($string));
	if(countUppercase > 2) $string = (strtolower($string));
	return ucfirst($string);
	
}

function countUppercase($string) {
// http://stackoverflow.com/questions/1182013/php-count-uppercase-words-in-string
// "/\b[A-Z][A-Za-z0-9]+\b/"
$arr = array();
     return preg_match_all("/\b[A-ZА-Я]+\b/", $string, $arr);
}

function xl2array($excelFile, $trimHeadTail = 0){
// Error reporting
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set("memory_limit","5956M");
// ini_set('display_errors', FALSE);
// ini_set('display_startup_errors', FALSE);
 
// Path to PHPExcel classes
require_once 'PHPExcel/Classes/PHPExcel.php';
require_once 'PHPExcel/Classes/PHPExcel/IOFactory.php';
 
 
 // require_once 'PHPExcel/IOFactory.php';



$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($excelFile);

$arrayData = array();
//Itrating through all the sheets in the excel workbook and storing the array data
// foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    /*$arrayData[$worksheet->getTitle()] = $worksheet->toArray();*/
	// array_push($arrayData, $worksheet->toArray() );
// }
$arrayData = $objPHPExcel->getActiveSheet()->toArray();
// var_dump($arrayData);
// $result = ($arrayData);

if ($trimHeadTail > 0){
unset($arrayData[0]);
}
if ($trimHeadTail == 2){
unset($arrayData[sizeof($arrayData)]);
}
return ($arrayData);
}//xl2array

function xl2array2($inputFileName,$dataonly=true,$headerstat=true,$row_callback=null){
    if (!class_exists('PHPExcel')) return false;
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly($dataonly);
        $objPHPExcel = $objReader->load($inputFileName);
		
    } catch(Exception $e) {
        return ('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
    }
    $sheet = $objPHPExcel->getSheet(0); 
    $highestRow = $sheet->getHighestRow(''); 
    $highestColumn = $sheet->getHighestColumn('');
	// $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
    $keys = array();
    $results = array();
    if(is_callable($row_callback)){
        for ($row = 1; $row <= $highestRow; $row++){ 
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,null,true,false);
            if ($row === 1){
                $keys = ($rowData[0]);
            } else {
                $record = array();
                foreach($rowData[0] as $pos=>$value) $record[$keys[$pos]] = $value; 
                $row_callback($record);           
            }
        } 
    } else {
        for ($row = 1; $row <= $highestRow; $row++){
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,null,true,false);
			// print_r($rowData);continue;
            if ($row === 1){
                $keys = ($rowData[0]);
				//remove empty columns
						foreach($keys as $k =>$v) {
						if($v == '' || empty($v)==true /*|| is_null($v)==true || isset($v)==true*/){ unset($keys[$k]);}
						}
						
				unset($keys['']);
				if($headerstat){
				$header = array();
                foreach($rowData[0] as $pos=>$value) $header[$keys[$pos]] = 0; 
				unset($header['']);

				$results[0] = array(); //
				// print_r($header); exit;
				// break;
				}
            } else {
                $record = array();
                foreach($rowData[0] as $pos=>$value) {
					$record[$keys[$pos]] = xl_fix($value);
					
					if($headerstat){ //!($value == '' || empty($value)==true) &&
						$clm_length = ( ($results[0][$keys[$pos]][0])< strlen($value)==1 ) ? strlen($value) : $results[0][$keys[$pos]][0] ;
						$is_numeric = ( ($results[0][$keys[$pos]][1])< is_numeric($value) ) ? is_numeric($value) : $results[0][$keys[$pos][1]] ;
						$is_decimal = ( ($results[0][$keys[$pos]][2])< is_decimal($value) ) ? is_decimal($value) : $results[0][$keys[$pos][2]] ;
						
						// if($pos == 7 && $row ==2 ){
							// print_r($header[$keys[$pos]]);
							// echo " header[keys[pos]]\n\r";
							// print_r($keys[$pos]." keys[$i]\n\r");
							// print_r(($value));
							// print_r($pos);
						// }

						$stat = array($clm_length,$is_numeric,$is_decimal);
						$results[0][$keys[$pos]] = $stat;
					}
					$i +=1;
					
				}
				
				unset($record['']);
				
				// if($headerstat){
					// foreach($record as $c=>$vl){
						
					// }
				// }
                $results[] = $record;  
				// if($row==3)break;
            }
        } 
		if($headerstat){
					// $results[0] = $header;
					}
		// print_r($keys);
		
		// $objPHPExcel->disconnectWorksheets();
		$objPHPExcel->__destruct();
		unset($objPHPExcel);
		
		unset($results[0]['']);

        return $results;
    }
}//xl2array2
function xl_fix($string){
	$string = str_ireplace("_\x000D_","",$string);
	$string = str_ireplace("_x000d_","",$string);
	$string = str_replace("\n","",$string);
	$string = str_replace("\r","",$string);
	
	
	return $string;
}


function xl2CSV($fileIn, $fileOut){
	// error_reporting(E_ALL);
// ini_set('display_errors', TRUE);
// ini_set('display_startup_errors', TRUE);
ini_set("memory_limit","5956M");
ini_set('max_execution_time', 300);
require 'PHPExcel/Classes/PHPExcel.php';
require_once 'PHPExcel/Classes/PHPExcel/IOFactory.php';

	$inputFileType = 'excel2007';
	$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	// $inputFileType = PHPExcel_IOFactory::identify($fileIn);
	// $objReader = PHPExcel_IOFactory::createReader($inputFileType);
	$objPHPExcel = $objReader->load($fileIn);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
	$objWriter->save($fileOut);
}
/*
$file = "list.txt";// Your Temp Uploaded file
$cols = array();
ini_set('auto_detect_line_endings', true);

$fh = fopen($file, 'r');
$i = 0;

while ($line = fgetcsv($fh, 1000, "\t") !== false) {
  $cols[] = $line;

  if($i == 19) { break; }
    $i++;
  }

echo "<pre>";
print_r($cols); 
echo "</pre>";
*/
function tab2array($file){
	return file_get_contents($file);
	$result = array();
$fp = fopen($file,'r');
if (($headers = fgetcsv($fp, 0, "\t")) !== FALSE)
  if ($headers)
    while (($line = fgetcsv($fp, 0, "\t")) !== FALSE) 
      if ($line)
        if (sizeof($line)==sizeof($headers))
          $result[] = array_combine($headers,$line);
fclose($fp);
return($result);
}

function index2assoc(array $ar, $key){
	$res = array();
	foreach($ar as $k => $v){
		$res[$v[$key]] = $v;
		// unset($res[$v[$key]][$key]);
	}
	
	return $res;
}

function ftpsend($ftp_server, $ftp_user_name, $ftp_user_pass, $path, $file){

$remote_file = $path."/".$file;
// $remote_file = str_ireplace("\\', "\", $remote_file);
$remote_file = str_ireplace("//", "/", $remote_file);

// установка соединения
$conn_id = ftp_connect($ftp_server);

// проверка имени пользователя и пароля
$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

// загрузка файла 
if (ftp_put($conn_id, $remote_file, $file, FTP_ASCII)) {
 echo "$file успешно загружен на сервер\n";
} else {
 echo "Не удалось загрузить $file на сервер\n";
}

// закрытие соединения
ftp_close($conn_id);
	
}//ftpsend


function ftpdownload($dbserver){
// $filenb = $dbserver['local_path'];
$dbserver['local_path'] =  mb_convert_encoding($dbserver['local_path'], "windows-1251", "utf-8");
$dbserver['ftp_file'] =  mb_convert_encoding($dbserver['ftp_file'], "windows-1251", "utf-8");

$conn_id = ftp_connect($dbserver['ftp_server']);

$login_result = ftp_login($conn_id, $dbserver['ftp_user_name'], $dbserver['ftp_user_pass']);

$check_file_exists = $dbserver['ftp_path'].$dbserver['ftp_file'];

$contents_on_server = ftp_nlist($conn_id, $dbserver['ftp_path']); //Returns an array of filenames from the specified directory on success or FALSE on error. 

$buff = ftp_mdtm($conn_id, $dbserver['ftp_path'].$dbserver['ftp_file']); //modification time

// if (!in_array($check_file_exists, $contents_on_server )=== FALSE) {
if ($buff != -1) {

if (ftp_get($conn_id, $dbserver['local_path'], $dbserver['ftp_path'].$dbserver['ftp_file'], FTP_BINARY)) {
    $result = 1; // "File $filenb downloaded \n\r";
} else {
    $result = 0; // echo "Не удалось завершить операцию\n\r";
}//if ftp_get

}else{//no such file
$result = -1;
}//if $check_file_exists


ftp_close($conn_id); 
return $result;
}

function fopen_utf8($filename){
	$encoding='';
	$handle = fopen($filename, 'r');
	$bom = fread($handle, 2);
//	fclose($handle);
	rewind($handle);
	
	if($bom === chr(0xff).chr(0xfe)  || $bom === chr(0xfe).chr(0xff)){
			// UTF16 Byte Order Mark present
			$encoding = 'UTF-16';
	} else {
		$file_sample = fread($handle, 1000) + 'e'; //read first 1000 bytes
		// + e is a workaround for mb_string bug
		rewind($handle);
	
		$encoding = mb_detect_encoding($file_sample , 'UTF-8, UTF-7, ASCII, EUC-JP,SJIS, eucJP-win, SJIS-win, JIS, ISO-2022-JP');
	}
	if ($encoding){
		stream_filter_append($handle, 'convert.iconv.'.$encoding.'/UTF-8');
	}
	return  ($handle);
} 

function removeBomUtf8($s){
  if(substr($s,0,3)==chr(hexdec('EF')).chr(hexdec('BB')).chr(hexdec('BF'))){
       return substr($s,3);
   }else{
       return $s;
   }
}

/*
ALTER TABLE
 MODIFY dt_modified datetime DEFAULT ON UPDATE CURRENT_TIMESTAMP
 ======================================================================
 
 
 
 
ALTER TABLE `oc_product` 
ADD COLUMN `category_id` int(11) DEFAULT NULL AFTER `date_modified`;

ALTER TABLE `oc_product` 
ADD COLUMN `expires` DATE NULL AFTER `category_id`;

ALTER TABLE `oc_product` 
ADD COLUMN `code` VARCHAR(64) NULL AFTER `sku`;

ALTER TABLE `oc_product` 
ADD COLUMN `name` VARCHAR(255) NULL AFTER `code`;

ALTER TABLE `oc_product` 
ADD `main_category` tinyint(1) NOT NULL DEFAULT '0';


ALTER TABLE `oc_product_description` 
ADD COLUMN `code` varchar(64) DEFAULT NULL AFTER `meta_keyword`;


	ALTER TABLE `oc_manufacturer` 
ADD COLUMN `date_added` DATETIME NULL AFTER `sort_order`;
	
	ALTER TABLE `oc_manufacturer_description` 
ADD COLUMN `date_modified` DATETIME NULL AFTER `meta_keyword`;	



ALTER TABLE `oc_product_to_category` ADD `main_category` tinyint(1) NOT NULL DEFAULT '0';
ALTER TABLE `oc_product_to_category` ADD INDEX `main_category` (`main_category`);	


--

ALTER TABLE `oc_ocstore_pickup` 
ADD COLUMN `meta_title` VARCHAR(255) NULL AFTER `sortorder`,
ADD COLUMN `meta_h1` VARCHAR(255) NULL AFTER `meta_title`,
ADD COLUMN `meta_description` VARCHAR(255) NULL AFTER `meta_h1`,
ADD COLUMN `meta_keyword` VARCHAR(255) NULL AFTER `meta_description`;

ALTER TABLE `oc_ocstore_pickup`
ADD `metro_id` tinyint(1) NOT NULL DEFAULT '0';

ALTER TABLE `u0193535_oc`.`oc_url_alias` 
ADD COLUMN `qold` VARCHAR(255) NULL AFTER `keyword`;







ALTER TABLE `u0193535_oc`.`oc_spravka_drugs` 
CHANGE COLUMN `DRUG_NAME` `DRUG_NAME_manufacturer` TEXT NULL DEFAULT NULL ,
ADD COLUMN `DRUG_NAME` VARCHAR(45) NULL AFTER `manufacturer`,
ADD COLUMN `manufacturer` VARCHAR(45) NULL AFTER `DRUG_NAME_manufacturer`;

ALTER TABLE `u0193535_oc`.`oc_spravka_drugs` 
ADD COLUMN `manufacturer_id` INT(11) NULL AFTER `DRUG_NAME`;


//-------------------------------------------------------------

https://stackoverflow.com/questions/4847089/mysql-joins-and-count-from-another-table
SELECT g.group_id, COUNT(m.member_id) AS members
FROM groups AS g
LEFT JOIN group_members AS m USING(group_id)
GROUP BY g.group_id
HAVING members > 4





ALTER TABLE `u0193535_oc`.`oc_module` 
ADD COLUMN `setting_archive` TEXT NULL AFTER `setting`;


ALTER TABLE `u0193535_oc`.`oc_ocstore_pickup` 
ADD COLUMN `spravka_price_id` INT(11) NULL AFTER `metro_id`;


CREATE TABLE `oc_product_update_log` (
  `update_id` int(11) NOT NULL,
  `new` int(4) DEFAULT NULL,
  `records` int(11) DEFAULT NULL,
  `barcodes` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  PRIMARY KEY (`update_id`)
) ;


ALTER TABLE `oc_review` 
ADD COLUMN `ip` VARCHAR(40) NULL AFTER `date_modified`,
ADD COLUMN `forwarded_ip` VARCHAR(40) NULL AFTER `ip`;




CREATE TABLE oc_specials LIKE oc_news; 
CREATE TABLE oc_specials_description LIKE oc_news_description; 



ALTER TABLE `u0193535_oc`.`oc_url_alias` 
ADD COLUMN `date_modified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `qold`,
ADD COLUMN `date_added` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `date_modified`;


ALTER TABLE  `oc_banner_image` 
ADD COLUMN `date_stop` DATE NULL DEFAULT NULL  AFTER `sort_order`; -- '0000-00-00'


ALTER TABLE `oc_news_description` 
ADD COLUMN `news` SMALLINT NULL AFTER `short_description`;


ALTER TABLE  `oc_product` 
ADD COLUMN `nnt` INT(11) NULL AFTER `expires`;

*/

?>