/* Polyfill service v3.20.2
 * For detailed credits and licence information see https://github.com/financial-times/polyfill-service.
 * 
 * UA detected: firefox/55.0.0
 * Features requested: Intl.~locale.ru,String.prototype.endsWith
 *  */

/* No polyfills found for current settings */
