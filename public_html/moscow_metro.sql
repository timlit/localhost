-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Май 07 2015 г., 11:19
-- Версия сервера: 5.5.8-CoreSeek
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `loc_metro`
--

CREATE TABLE IF NOT EXISTS `oc_ocloc_metro` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `fid` smallint(5) unsigned ,
  `loc_metro_line_id` varchar(40) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `title_en` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`),
  KEY `loc_metro_line_id` (`loc_metro_line_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=610 ;

--
-- Дамп данных таблицы `loc_metro`
--

INSERT INTO `oc_ocloc_metro` (`fid`, `loc_metro_line_id`, `title`, `title_en`) VALUES
(1, '1', 'Бульвар Рокоссовского', 'Bulvar Rokossovskogo'),
(2, '1', 'Черкизовская', 'Cherkizovskaya'),
(3, '1', 'Преображенская площадь', 'Preobrazhenskaya Ploshchad'),
(4, '1', 'Сокольники', 'Sokolniki'),
(5, '1', 'Красносельская', 'Krasnoselskaya'),
(6, '1,5', 'Комсомольская', 'Komsomolskaya'),
(7, '1', 'Красные ворота', 'Krasnye Vorota'),
(8, '1', 'Чистые пруды', 'Chistye Prudy'),
(9, '1', 'Лубянка', 'Lubyanka'),
(10, '1', 'Охотный ряд', 'Okhotny Ryad'),
(11, '1', 'Библиотека имени Ленина', 'Biblioteka Imeni Lenina'),
(12, '1', 'Кропоткинская', 'Kropotkinskaya'),
(13, '1,5', 'Парк культуры', 'Park Kultury'),
(14, '1', 'Фрунзенская', 'Frunzenskaya'),
(15, '1', 'Спортивная', 'Sportivnaya'),
(16, '1', 'Воробьёвы горы', 'Vorobyovy Gory'),
(17, '1', 'Университет', 'Universitet'),
(18, '1', 'Проспект Вернадского', 'Prospekt Vernadskogo'),
(19, '1', 'Юго-Западная', 'Yugo-Zapadnaya'),
(20, '1', 'Тропарёво', 'Troparyovo'),
(21, '7', 'Планерная', 'Planernaya'),
(434, '2', 'Речной вокзал', 'Rechnoy Vokzal'),
(435, '2', 'Водный стадион', 'Vodny Stadion'),
(436, '2', 'Войковская', 'Voykovskaya'),
(437, '2', 'Сокол', 'Sokol'),
(438, '2', 'Аэропорт', 'Aeroport'),
(439, '2', 'Динамо', 'Dinamo'),
(440, '2,5', 'Белорусская', 'Belorusskaya'),
(441, '2', 'Маяковская', 'Mayakovskaya'),
(442, '2', 'Тверская', 'Tverskaya'),
(443, '2', 'Театральная', 'Teatralnaya'),
(444, '2', 'Новокузнецкая', 'Novokuznetskaya'),
(445, '2,5', 'Павелецкая', 'Paveletskaya'),
(446, '2', 'Автозаводская', 'Avtozavodskaya'),
(447, '2', 'Коломенская', 'Kolomenskaya'),
(448, '2,11', 'Каширская', 'Kashirskaya'),
(449, '2', 'Кантемировская', 'Kantemirovskaya'),
(450, '2', 'Царицыно', 'Tsaritsyno'),
(451, '2', 'Орехово', 'Orekhovo'),
(452, '2', 'Домодедовская', 'Domodedovskaya'),
(453, '2', 'Красногвардейская', 'Krasnogvardeyskaya'),
(454, '2', 'Алма-Атинская', 'Alma-Atinskaya'),
(455, '3', 'Пятницкое шоссе', 'Pyatnitskoye Shosse'),
(456, '3', 'Митино', 'Mitino'),
(457, '3', 'Волоколамская', 'Volokolamskaya'),
(458, '3', 'Мякинино', 'Myakinino'),
(459, '3', 'Строгино', 'Strogino'),
(460, '3', 'Крылатское', 'Krylatskoye'),
(461, '3', 'Молодёжная', 'Molodyozhnaya'),
(462, '3,4', 'Кунцевская', 'Kuntsevskaya'),
(463, '3', 'Славянский бульвар', 'Slavyansky Bulvar'),
(464, '3,8', 'Парк Победы', 'Park Pobedy'),
(465, '3,4,5', 'Киевская', 'Kiyevskaya'),
(466, '3,4', 'Смоленская', 'Smolenskaya'),
(467, '3,4', 'Арбатская', 'Arbatskaya'),
(468, '3', 'Площадь Революции', 'Ploshchad Revolyutsii'),
(469, '3,5', 'Курская', 'Kurskaya'),
(470, '3', 'Бауманская', 'Baumanskaya'),
(471, '3', 'Электрозаводская', 'Elektrozavodskaya'),
(472, '3', 'Семёновская', 'Semyonovskaya'),
(473, '3', 'Партизанская', 'Partizanskaya'),
(474, '3', 'Измайловская', 'Izmaylovskaya'),
(475, '3', 'Первомайская', 'Pervomayskaya'),
(476, '3', 'Щёлковская', 'Shchyolkovskaya'),
(478, '4', 'Пионерская', 'Pionerskaya'),
(479, '4', 'Филёвский парк', 'Filyovsky Park'),
(480, '4', 'Багратионовская', 'Bagrationovskaya'),
(481, '4', 'Фили', 'Fili'),
(482, '4', 'Кутузовская', 'Kutuzovskaya'),
(483, '4', 'Студенческая', 'Studencheskaya'),
(484, '4', 'Международная', 'Mezhdunarodnaya'),
(485, '4', 'Выставочная', 'Vystavochnaya'),
(489, '4', 'Александровский сад', 'Aleksandrovsky Sad'),
(491, '5', 'Краснопресненская', 'Krasnopresnenskaya'),
(493, '5', 'Новослободская', 'Novoslobodskaya'),
(494, '5,6', 'Проспект Мира', 'Prospekt Mira'),
(497, '5,7', 'Таганская', 'Taganskaya'),
(499, '5', 'Добрынинская', 'Dobryninskaya'),
(500, '5,6', 'Октябрьская', 'Oktyabrskaya'),
(502, '6', 'Медведково', 'Medvedkovo'),
(503, '6', 'Бабушкинская', 'Babushkinskaya'),
(504, '6', 'Свиблово', 'Sviblovo'),
(505, '6', 'Ботанический сад', 'Botanichesky Sad'),
(506, '6', 'ВДНХ', 'VDNKh'),
(507, '6', 'Алексеевская', 'Alekseyevskaya'),
(508, '6', 'Рижская', 'Rizhskaya'),
(510, '6', 'Сухаревская', 'Sukharevskaya'),
(511, '6', 'Тургеневская', 'Turgenevskaya'),
(512, '6,7', 'Китай-город', 'Kitay-gorod'),
(513, '6,8', 'Третьяковская', 'Tretyakovskaya'),
(515, '6', 'Шаболовская', 'Shabolovskaya'),
(516, '6', 'Ленинский проспект', 'Leninsky Prospekt'),
(517, '6', 'Академическая', 'Akademicheskaya'),
(518, '6', 'Профсоюзная', 'Profsoyuznaya'),
(519, '6', 'Новые Черёмушки', 'Novye Cheryomushki'),
(520, '6', 'Калужская', 'Kaluzhskaya'),
(521, '6', 'Беляево', 'Belyayevo'),
(522, '6', 'Коньково', 'Konkovo'),
(523, '6', 'Тёплый Стан', 'Tyoply Stan'),
(524, '6', 'Ясенево', 'Yasenevo'),
(525, '6', 'Новоясеневская', 'Novoyasenevskaya'),
(527, '7', 'Сходненская', 'Skhodnenskaya'),
(528, '7', 'Тушинская', 'Tushinskaya'),
(529, '7', 'Спартак', 'Spartak'),
(530, '7', 'Щукинская', 'Shchukinskaya'),
(531, '7', 'Октябрьское поле', 'Oktyabrskoye Pole'),
(532, '7', 'Полежаевская', 'Polezhayevskaya'),
(533, '7', 'Беговая', 'Begovaya'),
(534, '7', 'Улица 1905 года', 'Ulitsa 1905 Goda'),
(535, '7', 'Баррикадная', 'Barrikadnaya'),
(536, '7', 'Пушкинская', 'Pushkinskaya'),
(537, '7', 'Кузнецкий мост', 'Kuznetsky Most'),
(540, '7', 'Пролетарская', 'Proletarskaya'),
(541, '7', 'Волгоградский проспект', 'Volgogradsky Prospekt'),
(542, '7', 'Текстильщики', 'Tekstilshchiki'),
(543, '7', 'Кузьминки', 'Kuzminki'),
(544, '7', 'Рязанский проспект', 'Ryazansky Prospekt'),
(545, '7', 'Выхино', 'Vykhino'),
(546, '7', 'Лермонтовский проспект', 'Lermontovsky Prospekt'),
(547, '7', 'Жулебино', 'Zhulebino'),
(549, '8', 'Деловой центр', 'Delovoy Tsentr'),
(551, '8', 'Марксистская', 'Marksistskaya'),
(552, '8', 'Площадь Ильича', 'Ploshchad Ilyicha'),
(553, '8', 'Авиамоторная', 'Aviamotornaya'),
(554, '8', 'Шоссе Энтузиастов', 'Shosse Entuziastov'),
(555, '8', 'Перово', 'Perovo'),
(556, '8', 'Новогиреево', 'Novogireyevo'),
(557, '8', 'Новокосино', 'Novokosino'),
(558, '9', 'Алтуфьево', 'Altufyevo'),
(559, '9', 'Бибирево', 'Bibirevo'),
(560, '9', 'Отрадное', 'Otradnoye'),
(561, '9', 'Владыкино', 'Vladykino'),
(562, '9', 'Петровско-Разумовская', 'Petrovsko-Razumovskaya'),
(563, '9', 'Тимирязевская', 'Timiryazevskaya'),
(564, '9', 'Дмитровская', 'Dmitrovskaya'),
(565, '9', 'Савёловская', 'Savyolovskaya'),
(566, '9', 'Менделеевская', 'Mendeleyevskaya'),
(567, '9', 'Цветной бульвар', 'Tsvetnoy Bulvar'),
(568, '9', 'Чеховская', 'Chekhovskaya'),
(569, '9', 'Боровицкая', 'Borovitskaya'),
(570, '9', 'Полянка', 'Polyanka'),
(571, '9', 'Серпуховская', 'Serpukhovskaya'),
(572, '9', 'Тульская', 'Tulskaya'),
(573, '9', 'Нагатинская', 'Nagatinskaya'),
(574, '9', 'Нагорная', 'Nagornaya'),
(575, '9', 'Нахимовский проспект', 'Nakhimovsky Prospekt'),
(576, '9', 'Севастопольская', 'Sevastopolskaya'),
(577, '9', 'Чертановская', 'Chertanovskaya'),
(578, '9', 'Южная', 'Yuzhnaya'),
(579, '9', 'Пражская', 'Prazhskaya'),
(580, '9', 'Улица академика Янгеля', 'Ulitsa Akademika Yangelya'),
(581, '9', 'Аннино', 'Annino'),
(582, '9', 'Бульвар Дмитрия Донского', 'Bulvar Dmitriya Donskogo'),
(583, '10', 'Марьина Роща', 'Maryina Roshcha'),
(584, '10', 'Достоевская', 'Dostoyevskaya'),
(585, '10', 'Трубная', 'Trubnaya'),
(586, '10', 'Сретенский бульвар', 'Sretensky Bulvar'),
(587, '10', 'Чкаловская', 'Chkalovskaya'),
(588, '10', 'Римская', 'Rimskaya'),
(589, '10', 'Крестьянская застава', 'Krestyanskaya Zastava'),
(590, '10', 'Дубровка', 'Dubrovka'),
(591, '10', 'Кожуховская', 'Kozhukhovskaya'),
(592, '10', 'Печатники', 'Pechatniki'),
(593, '10', 'Волжская', 'Volzhskaya'),
(594, '10', 'Люблино', 'Lyublino'),
(595, '10', 'Братиславская', 'Bratislavskaya'),
(596, '10', 'Марьино', 'Maryino'),
(597, '10', 'Борисово', 'Borisovo'),
(598, '10', 'Шипиловская', 'Shipilovskaya'),
(599, '10', 'Зябликово', 'Zyablikovo'),
(601, '11', 'Варшавская', 'Varshavskaya'),
(602, '11', 'Каховская', 'Kakhovskaya'),
(603, '12', 'Битцевский парк', 'Bittsevsky Park'),
(604, '12', 'Лесопарковая', 'Lesoparkovaya'),
(605, '12', 'Улица Старокачаловская', 'Ulitsa Starokachalovskaya'),
(606, '12', 'Улица Скобелевская', 'Ulitsa Skobelevskaya'),
(607, '12', 'Бульвар Адмирала Ушакова', 'Bulvar Admirala Ushakova'),
(608, '12', 'Улица Горчакова', 'Ulitsa Gorchakova'),
(609, '12', 'Бунинская аллея', 'Buninskaya Alleya');

-- --------------------------------------------------------

--
-- Структура таблицы `loc_metro_line`
--

CREATE TABLE IF NOT EXISTS `oc_ocloc_metro_line` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `title_en` varchar(255) NOT NULL DEFAULT '',
  `color` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `loc_metro_line`
--

INSERT INTO `oc_ocloc_metro_line` (`id`, `title`, `title_en`, `color`) VALUES
(1, 'Сокольническая', 'Sokolnicheskaya', '#ed1b35'),
(2, 'Замоскворецкая', 'Zamoskvoretskaya', '#44b85c'),
(3, 'Арбатско-Покровская', 'Arbatsko-Pokrovskaya', '#0078bf'),
(4, 'Филёвская', 'Filyovskaya', '#19c1f3'),
(5, 'Кольцевая', 'Koltsevaya', '#894e35'),
(6, 'Калужско-Рижская', 'Kaluzhsko-Rizhskaya', '#f58631'),
(7, 'Таганско-Краснопресненская', 'Tagansko-Krasnopresnenskaya', '#8e479c'),
(8, 'Калининско-Солнцевская', 'Kalininsko-Solntsevskaya', '#ffcb31'),
(9, 'Серпуховско-Тимирязевская', 'Serpukhovsko-Timiryazevskaya', '#a1a2a3'),
(10, 'Люблинско-Дмитровская', 'Lyublinsko-Dmitrovskaya', '#b3d445'),
(11, 'Каховская', 'Kakhovskaya', '#79cdcd'),
(12, 'Бутовская', 'Butovskaya', '#acbfe1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
