/*
UPDATE
u0193535_oc.oc_ocstore_pickup pick


left join u0193535_oc.oc_information_description infod
on pick.name = infod.meta_title
left join 
u0193535_oc.oc_url_alias a

-- 
on a.qold = concat("information_id=", information_id)
-- on a.query = concat ("address_id=", information_id)

SET
-- qold = a.query -- 
-- a.query =  replace(a.qold, 'information_id=' , 'address_id=')
-- a.query =  concat('address_id=' , pick.ocstore_pickup_id)
pick.meta_title = infod.title
WHERE qold like '%information_id=%'
;
*/
SELECT ocstore_pickup_id, infod.title, infod.information_id, a.query, a.keyword, a.qold
FROM
u0193535_oc.oc_ocstore_pickup pick

left join u0193535_oc.oc_information_description infod
on pick.name = infod.meta_title
left join u0193535_oc.oc_url_alias a
-- 
on a.qold = concat ("information_id=", information_id)
-- on a.qold = concat ("address_id=", information_id)

;


/*
UPDATE u0193535_oc.oc_ocstore_pickup pick
left join u0193535_oc.oc_information_description infod
on pick.name = infod.meta_title
left join u0193535_oc.oc_url_alias a
on a.query = concat ("information_id=", information_id)

SET 
pick.description = infod.description,
pick.meta_description = infod.meta_description,
pick.meta_h1 = infod.meta_h1,
pick.meta_keyword = infod.meta_keyword,
pick.meta_title = pick.meta_title

WHERE pick.description = '&lt;p&gt;&lt;br&gt;&lt;/p&gt;'
;
*/

/* 
SELECT * FROM u0193535_oc.oc_ocstore_pickup



where enable = 1 and ocstore_pickup_id <> 16
;
*/
