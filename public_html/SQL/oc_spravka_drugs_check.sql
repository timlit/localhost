SELECT *

,
@volume := 
   
case /* volume items*/
when locate("мг " , DRUG_NAME_manufacturer ) >0 then 'мг '
when locate("мг/мл " , DRUG_NAME_manufacturer ) >0 then 'мг/мл '
when locate("мл " , DRUG_NAME_manufacturer ) >0 then 'мл ' 
when locate("г " , DRUG_NAME_manufacturer ) >0 then 'г '
when locate("ед/мл " , DRUG_NAME_manufacturer ) >0 then 'ед/мл '
when locate(binary "ЕД " , DRUG_NAME_manufacturer ) >0 then 'ЕД '

when locate("бл " , DRUG_NAME_manufacturer ) >0 then 'бл '
when locate("бан " , DRUG_NAME_manufacturer ) >0 then 'бан '
when locate("капли " , DRUG_NAME_manufacturer ) >0 then 'капли '
when locate("сироп " , DRUG_NAME_manufacturer ) >0 then 'сироп '
when locate("крем " , DRUG_NAME_manufacturer ) >0 then 'крем '
when locate("амп " , DRUG_NAME_manufacturer ) >0 then 'амп '


else ' '

end
   
   as volume



, 
@volume_items := 
case /* volume items*/
when @volume <> ' '
 then 

SUBSTRING_INDEX  (
  SUBSTRING_INDEX (
DRUG_NAME_manufacturer,

@volume , 1) 
, ' ', -1 )

else ' '
end
as volume_items

, 
@items_count :=

  SUBSTRING_INDEX (
 SUBSTRING_INDEX (
  SUBSTRING_INDEX (DRUG_NAME_manufacturer, ' N', -1) 

, ' ', 1
 
 ),
 
'x', 1) 


*

-- SUBSTRING_INDEX (SUBSTRING_INDEX (DRUG_NAME_manufacturer, 'x', -1) , ' ', 7)
 SUBSTRING_INDEX (
 SUBSTRING_INDEX (
  SUBSTRING_INDEX (DRUG_NAME_manufacturer, ' N', -1) 

, ' ', 1
 
 ),
 
'x', -1) /* items_count */




as items_count


,
@pattern := 
concat(
SUBSTRING_INDEX (DRUG_NAME_manufacturer, ' ', 1)
, ' %', @volume_items , @volume, '%№', @items_count)

 as pattern2

 FROM direct.oc_spravka_drugs
 
 
 
 
;