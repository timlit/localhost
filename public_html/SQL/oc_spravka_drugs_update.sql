/* 
UPDATE oc_spravka_drugs

set 

DRUG_NAME = 
replace(
replace(
DRUG_NAME_manufacturer,

if( locate(" N" , DRUG_NAME_manufacturer ) > 0 ,

trim(replace(
 SUBSTRING_INDEX (SUBSTRING_INDEX (DRUG_NAME_manufacturer, 'x', -1) , ' ', 70)
,
 SUBSTRING_INDEX (SUBSTRING_INDEX (DRUG_NAME_manufacturer, 'x', -1) , ' ', 1) 
 ,''))
 
 
, "")
,''

)

,'""','"')
,

manufacturer = 
-- 
replace(
if( locate(" N" , DRUG_NAME_manufacturer ) > 0 ,

trim(replace(
 SUBSTRING_INDEX (SUBSTRING_INDEX (DRUG_NAME_manufacturer, 'x', -1) , ' ', 7)
,
 SUBSTRING_INDEX (SUBSTRING_INDEX (DRUG_NAME_manufacturer, 'x', -1) , ' ', 1) 
 ,''))
 
 
, "")

-- 
,'"','')

WHERE
drugs.manufacturer_id is null

;

*/

/* 
UPDATE oc_spravka_drugs as drugs

left join oc_manufacturer_new manufnew
 -- on drugs.﻿manufacturer_id = manufnew.manufacturer_id
 on 
drugs.manufacturer = manufnew.name
set 
drugs.manufacturer_id = manufnew.manufacturer_id


;
*/

update oc_spravka_drugs

set
pattern =
/* pattern search */
concat(
SUBSTRING_INDEX (DRUG_NAME_manufacturer, ' ', 1),
' %',


 (
  SUBSTRING_INDEX (

case 
when locate("мг " , DRUG_NAME_manufacturer ) >0 then 'мг '
when locate("мг/мл " , DRUG_NAME_manufacturer ) >0 then 'мг/мл '
when locate("мл " , DRUG_NAME_manufacturer ) >0 then 'мл ' 
when locate("г " , DRUG_NAME_manufacturer ) >0 then 'г '
when locate("ед/мл " , DRUG_NAME_manufacturer ) >0 then 'ед/мл '
when locate(binary "ЕД " , DRUG_NAME_manufacturer ) >0 then 'ЕД '

when locate("бл " , DRUG_NAME_manufacturer ) >0 then 'бл '
when locate("бан " , DRUG_NAME_manufacturer ) >0 then 'бан '
when locate("капли " , DRUG_NAME_manufacturer ) >0 then 'капли '
when locate("сироп " , DRUG_NAME_manufacturer ) >0 then 'сироп '
when locate("крем " , DRUG_NAME_manufacturer ) >0 then 'крем '
when locate("амп " , DRUG_NAME_manufacturer ) >0 then 'амп '
else ''

end

  
  
  , ' ', 1) 

-- , 'г ', -1
 
 )

,

'%№',

 SUBSTRING_INDEX (
 SUBSTRING_INDEX (
  SUBSTRING_INDEX (DRUG_NAME_manufacturer, ' N', -1) 

, ' ', 1
 
 ),
 
'x', 1) 


*

-- SUBSTRING_INDEX (SUBSTRING_INDEX (DRUG_NAME_manufacturer, 'x', -1) , ' ', 7)
 SUBSTRING_INDEX (
 SUBSTRING_INDEX (
  SUBSTRING_INDEX (DRUG_NAME_manufacturer, ' N', -1) 

, ' ', 1
 
 ),
 
'x', -1) /* items_count */








) /* pattern search */