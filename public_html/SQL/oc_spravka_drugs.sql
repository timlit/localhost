-- delete from oc_spravka_drugs WHERE DRUG_NAME_manufacturer like "%кг%";


-- SELECT *, SUBSTRING_INDEX ( SUBSTRING_INDEX (SUBSTRING_INDEX (DRUG_NAME_manufacturer, "x", -1) , " ", 5), " ", 5) as manuf -- 
SELECT DISTINCT @name_new := drugs.DRUG_NAME_manufacturer as name_new
,pd.name as name_infoapteka
/* product description

,pd.description
,pd.meta_description
,pd.meta_h1
,pd.meta_keyword
,pd.meta_title
 */
, 
@countN :=
locate(" N" , @name_new ) 
>0
/*
if 
(
locate(" N" , drugs.DRUG_NAME_manufacturer ) 
>0, 1, 0
)*/
as countN

, 
@countN2 :=
locate(" №" , pd.name ) 
>0
/*
if 
(
locate(" N" , drugs.DRUG_NAME_manufacturer ) 
>0, 1, 0
)*/
as countN2

,

@density := 
   
case /* density */
when locate(binary "ЕД/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "ЕД/мл "
when locate(binary "МЕ/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "МЕ/мл "
when locate(binary "ЕД " , drugs.DRUG_NAME_manufacturer ) >0  then "ЕД/мл "
when locate(binary "МЕ " , drugs.DRUG_NAME_manufacturer ) >0 then "МЕ/мл "
when locate("ед/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "ед/мл "
when locate("мг/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "мг/мл "


else " "

end
   
   as density
,


@density2 := 
   
case /* density */
when locate(binary "ЕД/мл " , pd.name ) >0 then "ЕД/мл "
when locate(binary "МЕ/мл " , pd.name ) >0 then "МЕ/мл "
when locate(binary "ЕД " , pd.name ) >0  then "ЕД/мл "
when locate(binary "МЕ " , pd.name ) >0 then "МЕ/мл "
when locate("ед/мл " , pd.name ) >0 then "ед/мл "
when locate("мг/мл " , pd.name ) >0 then "мг/мл "


else " "

end
   
   as density2
,


@density_items := 
case /* density items*/
when @density <> " "
 then 

SUBSTRING_INDEX  (
  SUBSTRING_INDEX (
drugs.DRUG_NAME_manufacturer,

@density , 1) 
, " ", -1 )

else " "
end
as density_items

,


@density_items2 := 
case /* density items*/
when @density2 <> " "
 then 

SUBSTRING_INDEX  (
  SUBSTRING_INDEX (
pd.name,

@density2 , 1) 
, " ", -1 )

else " "
end
as density_items2
,

@volume := 
   
case /* volume items*/
/*
when locate("мг " , drugs.DRUG_NAME_manufacturer ) >0 then "мг "
when locate("мг/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "мг/мл "
when locate("мл " , drugs.DRUG_NAME_manufacturer ) >0 then "мл " 
when locate("г " , drugs.DRUG_NAME_manufacturer ) >0 then "г "
when locate("ед/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "ед/мл "
when locate(binary "ЕД " , drugs.DRUG_NAME_manufacturer ) >0  or locate(binary "МЕ " , drugs.DRUG_NAME_manufacturer ) >0 then "ЕД "
*/

when locate("мг " , replace(@name_new, "", "") ) >0 then "мг "
when locate("мл " , replace(@name_new, binary "ЕД/мл", "") ) >0 then "мл " 
when locate("г " , replace(@name_new, "", "") ) >0 then "г "

else " "

end
   
   as vl
   
,

@volume2 := 
   
case /* volume items*/
when locate("мг " , pd.name ) >0 then "мг "
when locate("мл " , pd.name ) >0 then "мл " 
when locate("г " , pd.name ) >0 then "г "

else " "

end
   
   as vl2
   
,

@form := 
   
case /* form */

when locate("бл " , drugs.DRUG_NAME_manufacturer ) >0 then "бл "
when locate("бан " , drugs.DRUG_NAME_manufacturer ) >0 then "бан "
when locate("капли " , drugs.DRUG_NAME_manufacturer ) >0 then "капли "
when locate("сироп " , drugs.DRUG_NAME_manufacturer ) >0 then "сироп "
when locate("крем " , drugs.DRUG_NAME_manufacturer ) >0 then "крем "
when locate("амп " , drugs.DRUG_NAME_manufacturer ) >0 then "амп "


else " "

end
   
   as form

,

@form2 := 
   
case /* form2 */

when locate("бл " , pd.name ) >0 then "бл "
when locate("бан " , pd.name ) >0 then "бан "
when locate("капли " , pd.name ) >0 then "капли "
when locate("сироп " , pd.name ) >0 then "сироп "
when locate("крем " , pd.name ) >0 then "крем "
when locate("амп " , pd.name ) >0 then "амп "


else " "

end
   
   as form2

, 
@volume_items := 
case /* volume items*/
when @volume <> " "
 then 

SUBSTRING_INDEX  (
  SUBSTRING_INDEX (
replace(@name_new,  "/мл", "") ,

@volume , 1) 
, " ", -1 )

else " "
end
as vi


, 
@volume_items2 := 
case /* volume items*/
when @volume2 <> " "
 then 

SUBSTRING_INDEX  (
  SUBSTRING_INDEX (

replace(pd.name,  "/мл", "") ,

@volume2 , 1) 
, " ", -1 )

else " "
end
as vi2


, 
@items_count :=

  SUBSTRING_INDEX (
 SUBSTRING_INDEX (
  SUBSTRING_INDEX (drugs.DRUG_NAME_manufacturer, " N", -1) 

, " ", 1
 
 ),
 
"x", 1) 

*
-- SUBSTRING_INDEX (SUBSTRING_INDEX (DRUG_NAME_manufacturer, "x", -1) , " ", 7)
 SUBSTRING_INDEX (
 SUBSTRING_INDEX (
  SUBSTRING_INDEX (drugs.DRUG_NAME_manufacturer, " N", -1) 

, " ", 1
 
 ),
 
"x", -1) /* items_count */
as ic

, 
@items_count2 :=

  SUBSTRING_INDEX (
 SUBSTRING_INDEX (
  SUBSTRING_INDEX (pd.name, " №", -1) 

, " ", 1
 
 ),
 
"x", 1) 


as ic2
,
@pattern := -- https://www.danshort.com/ASCIImap/
replace (
concat(
SUBSTRING_INDEX (drugs.DRUG_NAME_manufacturer, " ", 1)
, " __prc", @volume_items , @volume, "__prcN", @items_count)
 , 
 ("__prc __prc")
 -- ("Аевит #prc  #prcN10")
 , char(37))
 as pattern
 
,

@pattern2 := 
replace (
concat(
SUBSTRING_INDEX (pd.name, " ", 1)
, " %", @volume_items2 , @volume2, "%№", @items_count2)
 , "% %", "")
 as pattern2

FROM oc_spravka_drugs drugs
 

left join oc_product_description pd
-- on pd.name like @pattern
on 
(
--  replace (@volume2, "МЕ", "ЕД")
-- countN = countN2 and
@volume_items = @volume_items2 and
@volume = @volume2 and

@items_count = @items_count2 
)


or
SUBSTRING_INDEX (drugs.DRUG_NAME_manufacturer, " ", 1) = SUBSTRING_INDEX (pd.name, " ", 1) and

pd.name like 
/* pattern search */
-- @pattern = @pattern2
concat(
SUBSTRING_INDEX (drugs.DRUG_NAME_manufacturer, " ", 1),
" %",

/* (
  SUBSTRING_INDEX (-- form 
case 

when locate("бл " , drugs.DRUG_NAME_manufacturer ) >0 then "бл "
when locate("бан " , drugs.DRUG_NAME_manufacturer ) >0 then "бан "
when locate("капли " , drugs.DRUG_NAME_manufacturer ) >0 then "капли "
when locate("сироп " , drugs.DRUG_NAME_manufacturer ) >0 then "сироп "
when locate("крем " , drugs.DRUG_NAME_manufacturer ) >0 then "крем "
when locate("амп " , drugs.DRUG_NAME_manufacturer ) >0 then "амп "
else ""

end
  , " ", 1) 

 )
 
, "%", */
 (
  SUBSTRING_INDEX (-- volume 

case 
when locate("мг " , drugs.DRUG_NAME_manufacturer ) >0 then "мг "
when locate("мг/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "мг/мл "
when locate("мл " , drugs.DRUG_NAME_manufacturer ) >0 then "мл " 
when locate("г " , drugs.DRUG_NAME_manufacturer ) >0 then "г "
when locate("ед/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "ед/мл "
when locate(binary "ЕД " , drugs.DRUG_NAME_manufacturer ) >0 then "ЕД "

else ""

end
  
  , " ", 1) 
 
 )

,

"%№",

 SUBSTRING_INDEX (
 SUBSTRING_INDEX (
  SUBSTRING_INDEX (DRUG_NAME_manufacturer, " N", -1) 

, " ", 1
 
 ),
 
"x", 1) 


*

-- SUBSTRING_INDEX (SUBSTRING_INDEX (DRUG_NAME_manufacturer, "x", -1) , " ", 7)
 SUBSTRING_INDEX (
 SUBSTRING_INDEX (
  SUBSTRING_INDEX (DRUG_NAME_manufacturer, " N", -1) 

, " ", 1
 
 ),
 
"x", -1) /* items_count */








) /* pattern search */












--  
WHERE  

-- DRUG_NAME_manufacturer like "%Ревлимид%"
/* or */
(locate(" N" , DRUG_NAME_manufacturer ) >0) 
-- and (locate("субст" , DRUG_NAME_manufacturer ) =0) 


-- AND @volume is not null -- no volume
/*
AND

 SUBSTRING_INDEX (
  
  SUBSTRING_INDEX (

case 
when locate("мг " , DRUG_NAME_manufacturer ) >0 then "мг"
when locate("мл " , DRUG_NAME_manufacturer ) >0 then "мл"
when locate("г " , DRUG_NAME_manufacturer ) >0 then "г"
when locate("мг " , DRUG_NAME_manufacturer ) >0 then "мг"
when locate("ед " , DRUG_NAME_manufacturer ) >0 then "ед"


end

  
  
  , "г ", 1) 

, "г ", -1
 
 ) is not null -- no volume
*/

/*
AND 
concat(
pd.code
,pd.description
,pd.meta_description
,pd.meta_h1
,pd.meta_keyword
,pd.meta_title
)

 is not null
*/

-- drugs.manufacturer_id is null


-- ORDER BY ﻿DRUG_ID asc
-- LIMIT 5000 OFFSET 50
 ;
 /*
 REPLACE ( 
 , REGEXP "[[:digit:]] ", "")
 
 */
 
 
 
 /*
 
 UPDATE oc_spravka_drugs as drugs


set

manufacturer = replace(manufacturer, ""","" )

;

 */
 
 
 
 /*
insert into oc_manufacturer_new

(name)

select DISTINCT replace(manufacturer, """, "") as manuf

 FROM u0193535_oc.oc_spravka_drugs
 
 
 
 WHERE manufacturer <> ""
 */
 --  and manufacturer like "%-%"
  
  /*
  -- find duplicate rows in table --
  SELECT firstname, 
   lastname, 
   list.address 
FROM list
   INNER JOIN (SELECT address
               FROM   list
               GROUP  BY address
               HAVING COUNT(id) > 1) dup
           ON list.address = dup.address;
  */