DELIMITER $$

DROP function IF EXISTS `sp_user_login` $$
CREATE 
-- DEFINER=`root`@`%` 
DEFINER= `u0193535_oc`@`%`
function `titlereplace`(
   txt VARCHAR(255)
  
) returns text CHARSET cp1251
BEGIN

 txt = replace(txt, 'пр-в', 'презерватив') ;


return txt;
END $$

DELIMITER ;


DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_user_login` $$
CREATE DEFINER=`u0193535_oc`@`%` PROCEDURE `sp_user_login`(
  IN loc_username VARCHAR(255),
  IN loc_password VARCHAR(255)
)
BEGIN

  SELECT user_id,
         user_name,
         user_emailid,
         user_profileimage,
         last_update
    FROM tbl_user
   WHERE user_name = loc_username
     AND password = loc_password
     AND status = 1;

END $$

DELIMITER ;