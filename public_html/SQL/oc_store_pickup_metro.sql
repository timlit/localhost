/* 
UPDATE
u0193535_oc.oc_ocstore_pickup pick

left join
oc_ocloc_metro stations
on pick.address like concat('%', stations.title,'%') 
SET
pick.metro_id =  (stations.id)

*/
;
SELECT  pick.name, pick.address, worktime, pick.phone, pick.email, stations.title, mlines.id as mlid, COUNT(DISTINCT mlist.id) as mcount, pick.metro_id, stations.id  FROM oc_ocstore_pickup pick

left join
oc_ocloc_metro stations
on pick.address like concat('%', stations.title,'%') 

left join 
oc_ocloc_metro_line mlines
on stations.loc_metro_line_id = mlines.id

left join 
oc_ocloc_metro as mlist
-- 
on mlist.id =  stations.loc_metro_line_id 
 
--  USING(mlist.id)



WHERE enable = 1 and ocstore_pickup_id <>16
-- AND description = '' or description is null

-- AND panorama is NULL
-- AND map is NULL
-- and email like '%mail.ru%'
-- 
GROUP BY pick.ocstore_pickup_id
-- GROUP BY stations.loc_metro_line_id
-- GROUP BY mlines.id
-- GROUP BY 

ORDER BY sortorder ASC

;
