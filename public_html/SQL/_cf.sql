DELIMITER !

DROP FUNCTION IF EXISTS alphas! 

CREATE FUNCTION alphas ( str VARCHAR(255) ) 
                   RETURNS VARCHAR(255) 
                   DETERMINISTIC
BEGIN 
  DECLARE i, len SMALLINT DEFAULT 1; 
  DECLARE ret VARCHAR(255) DEFAULT ''; 
  DECLARE c VARCHAR(1);   /* NOT CHAR!  NOT CHAR! NOT CHAR! */ 
  SET len = CHAR_LENGTH( str ); 
  REPEAT 
    BEGIN 
      SET c = MID( str, i, 1 ); 
      IF NOT c REGEXP '[[:digit:]]' THEN 
        SET ret=CONCAT(ret,c); 
      END IF; 
      SET i = i + 1; 
    END; 
  UNTIL i > len END REPEAT; 
  RETURN ret; 
END !
DELIMITER ; 



DELIMITER $$ 

DROP FUNCTION IF EXISTS `uExtractNumberFromString`$$
CREATE FUNCTION `uExtractNumberFromString`(in_string varchar(50)) 
RETURNS INT
NO SQL

BEGIN

    DECLARE ctrNumber varchar(50);
    DECLARE finNumber varchar(50) default ' ';
    DECLARE sChar varchar(2);
    DECLARE inti INTEGER default 1;

    IF length(in_string) > 0 THEN

        WHILE(inti <= length(in_string)) DO
            SET sChar= SUBSTRING(in_string,inti,1);
            SET ctrNumber= FIND_IN_SET(sChar,'0,1,2,3,4,5,6,7,8,9');

            IF ctrNumber > 0 THEN
               SET finNumber=CONCAT(finNumber,sChar);
            ELSE
               SET finNumber=CONCAT(finNumber,'');
            END IF;
            SET inti=inti+1;
        END WHILE;
        RETURN CAST(finNumber AS SIGNED INTEGER) ;
    ELSE
        RETURN 0;
    END IF;

END$$

DELIMITER ;