/*update u0193535_oc.oc_ocstore_pickup pick
left join u0193535_oc.oc_information_description infod
on pick.name = infod.meta_title
left join u0193535_oc.oc_url_alias a
on a.query = concat ("information_id=", information_id)

-- SET url = keyword
SET title = replace(infod.title, "Добрая Аптека - ", "")
;*/ -- pick.title, , pick.url, name
-- SELECT ocstore_pickup_id, name, phone, email, map, image, enable FROM u0193535_oc.oc_ocstore_pickup pick ORDER BY `pick`.`image` ASC 

SELECT 
-- name,
ocstore_pickup_id, infod.information_id, replace(infod.title, "Добрая Аптека - ", "") as title, address, worktime, concat("<a href='tel:", 
replace(replace(replace(replace(phone, "-", ""), " ", ""), "(", ""), ")", ""),
 "'>", 

replace(phone, " ", "")
 , "</a>") as phone, phone as tel,
concat('<a href="http://apteka-sale.ru/', keyword, '" target="_blank">', replace(infod.title, "Добрая Аптека - ", ""),'</a>') as link

,
concat('<div class="ctcs-block"><div class="lt"><div class="info"><a name="ap1"></a>
<div class="name">
', 
concat('<a href="http://apteka-sale.ru/', keyword, '>', replace(infod.title, "Добрая Аптека - ", ""),'</a>') 


, '" target="_blank">', replace(infod.title, "Добрая Аптека - ", ""), '</a></div>
<div>'
, address
,'</div>
<div class="phone">
<gisphone class="_gis-phone-highlight-wrap js_gis-phone-highlight-wrap-7738b46fe7646a822df6c6125d669d7c _gis-phone-highlight-phone-wrap" data-ph-parsed="true">
', concat("<a href='tel:", 
replace(replace(replace(replace(phone, "-", ""), " ", ""), "(", ""), ")", ""),  "'>", replace(phone, " ", "")  , "</a>")


,'</gisphone>
</div>

<div>Часы работы: </div>
<div>'
, worktime

,'</div>
<div><a class="colorbox fancybox schedule" href="/index.php?route=information/information/agree&amp;amp;information_id=17"><i class="fa fa-info-circle"></i> График работы аПтек в праздничные дни</a></div></div>

<div class="map">
' -- width=500&amp;height=400
, replace(map, "width=500&amp;height=400", "width=100%&amp;height=250")
,'</div>'
,replace('<div class="rt">
<div class="m-img"> 
<img alt="" src="/image/map/#image_2.jpg"> 
<img alt="" src="/image/map/#image_3.jpg"> 
<img alt="" src="/image/map/#image_4.jpg"> </div>
<div class="full-img"> <img alt="" src="/image/map/#image_1.jpg"> 
</div>
</div></div>', '#image', image) ) as description
 
, email, keyword, map, image, panorama, enable FROM u0193535_oc.oc_ocstore_pickup pick
left join u0193535_oc.oc_information_description infod
on pick.name = infod.meta_title
left join u0193535_oc.oc_url_alias a
on a.query = concat ("information_id=", information_id)

-- 
where enable = 1 and name like "%Добрая аптека%"
order by sortorder asc
;

/*
<script type="text/javascript" charset="utf-8" async 
src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A57e9164ff18236fe117fa2b96af9c1c8aee71033d5af01e117ee01650132b187&amp;width=500&amp;height=400&amp;lang=ru_RU&amp;scroll=true"
></script>
*/
