UPDATE oc_product as p
-- inner JOIN oc_product_to_category as c ON
-- p.product_id = c.product_id 


inner JOIN u0193535_oc.import d
ON p.sku = d.model


set p.quantity = d.quantity, p.code = d.code, p.price = d.price, p.date_modified = now()

-- 
where p.price <> d.price 
-- 
or p.quantity <> d.quantity

;


-- UPDATE my_table
-- SET col_Z = IF(col_A = 4 OR col_B = 4, 4, NULL)
-- WHERE id = "001"

