-- http://apteka-sale.ru/index.php?route=product/product&product_id=64134
SELECT date_added as data
-- , date_modified
-- , ip
-- , pick.phone
, o.order_id as zakaz
-- 
, pick.email
, op.product_id, op.name as tovar
, concat(o.total, " руб.") as itogo, firstname as Imya, o.email, telephone
-- , replace(shipping_code, 'ocstore_pickup.','') as site
-- , address

-- ,user_agent
 FROM u0193535_oc.oc_order o
 
 INNER join oc_order_product op
 on o.order_id = op.order_id

left join u0193535_oc.oc_ocstore_pickup pick
on replace(shipping_code, 'ocstore_pickup.','') = pick.ocstore_pickup_id
/*
where shipping_code = 'ocstore_pickup.5'
and ip <> "37.230.253.189"
*/

-- WHERE op.name like '%даклинза%'
-- pick.email  like '%sviblovo@mail.ru%'
-- pick.email like '%tim%'
group by o.order_id
order by date_added desc, pick.email asc, date_modified desc
-- LIMIT 50
;

