/*
update oc_review r

left join oc_ocstore_pickup pick
on lcase(r.pharmacy) = lcase(pick.name)

set pharmacy_id = pick.ocstore_pickup_id
;
*/
SELECT r.review_id, r.pharmacy_id, pick.meta_h1, r.customer_id, r.author, 
r.rating, r.status, r.text, r.date_added FROM u0193535_oc.oc_review r

left join oc_ocstore_pickup pick
on r.pharmacy_id = pick.ocstore_pickup_id



WHERE product_id = 0 -- and pharmacy_id = 1
ORDER BY review_id DESC
;