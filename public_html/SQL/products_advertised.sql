-- SELECT c.category_id as cid, cd.name, p.code, p.manufacturer_id as mid, md.name as manufacturer, p.product_id as pid, d.name,   status FROM u0193535_oc.oc_product p
SELECT c.category_id as cid, cd.name, p.manufacturer_id as mid, md.name as manufacturer, p.product_id as pid, d.name,   status FROM u0193535_oc.oc_product p
left join u0193535_oc.oc_product_description d
on p.product_id = d.product_id

left join 
u0193535_oc.oc_product_to_category c
on p.product_id = c.product_id

left join u0193535_oc.oc_category_description cd
on c.category_id = cd.category_id

left join u0193535_oc.oc_manufacturer_description md
on p.manufacturer_id = md.manufacturer_id

-- where manufacturer_id = 1939

-- where product_id = 62119

-- 
where 
-- image <> '' and status = 1
-- product_id = 60477 -- 62119
-- manufacturer_id = 1119
-- or manufacturer_id = 721

d.name like '%Абаджио%%'

or d.name like '%Абилифай%'

or d.name like '%Абраксан%'

or d.name like '%Авастин%'

or d.name like '%Акласта%'

or d.name like '%Алимта%'

or d.name like '%Аллокин_Альфа%'

or d.name like '%Альбумин%'

or d.name like '%Афинитор%'

or d.name like '%Ацеллбия%'

or d.name like '%Бараклюд%'

or d.name like '%Бевацизумаб%'

or d.name like '%Бейодайм%'

or d.name like '%Вайдаза%'

or d.name like '%Вектибикс%'

or d.name like '%Велкейд%'

or d.name like '%Викейра_Пак%'

or d.name like '%Виктоза%'

or d.name like '%Вотриент%'

or d.name like '%Герцептин%'

or d.name like '%Гиотриф%'

or d.name like '%Гливек%'

or d.name like '%Гонал%'

or d.name like '%Даклинза%'

or d.name like '%Джевтана%'

or d.name like '%Диферелин%'

or d.name like '%Долгит%'

or d.name like '%Доцетаксел%'

or d.name like '%Дьюралан%'

or d.name like '%Жавлор%'

or d.name like '%Залтрап%'

or d.name like '%Зелбораф%'

or d.name like '%Зивокс%'

or d.name like '%Зинфоро%'

or d.name like '%Зитига%'

or d.name like '%Зовиракс%'

or d.name like '%Золадекс%'

or d.name like '%Золинза%'

or d.name like '%Зомета%'

or d.name like '%Иматини%'

or d.name like '%Ингавирин%'

or d.name like '%Инлита%'

or d.name like '%Иресса%'

or d.name like '%Йонделис%'

or d.name like '%Кагоцел%'

or d.name like '%Кадсила%'

or d.name like '%Капрелса%'

or d.name like '%Касодекс%'

or d.name like '%Келикс%'

or d.name like '%Клексан%'

or d.name like '%Копаксон%'

or d.name like '%Кселода%'

or d.name like '%Лаеннек%'

or d.name like '%Мабтера%'

or d.name like '%Менопур%'

or d.name like '%Нексавар%'

or d.name like '%Неопакс%'

or d.name like '%Октагам%'

or d.name like '%Омнипак%'

or d.name like '%Онкотрон%'

or d.name like '%Остенил%'

or d.name like '%Полиорикс%'

or d.name like '%Пролиа%'

or d.name like '%Пурегон%'

or d.name like '%Ревлимид%'

or d.name like '%Регогафениб%'

or d.name like '%Ремикейд%'

or d.name like '%Совриад%'

or d.name like '%Спрайсел%'

or d.name like '%Стиварга%'

or d.name like '%Сунвепра%'

or d.name like '%Таксол%'

or d.name like '%Таксотер%'

or d.name like '%Тарцева%'

or d.name like '%Тасигна%'

or d.name like '%Таутакс%'

or d.name like '%Тафинлар%'

or d.name like '%Темомид%'

or d.name like '%Траумель%'

or d.name like '%Трувада%'

or d.name like '%Ферматрон%'

or d.name like '%Фраксипарин%'

or d.name like '%Хаврикс%'

or d.name like '%Халавен%'

or d.name like '%Хумира%'

or d.name like '%Эйлеа%'

or d.name like '%Эксджива%'

or d.name like '%Энбрел%'

or d.name like '%Яквинус%'

order by cd.name desc
;

