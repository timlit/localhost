update oc_product_description d
left join oc_product p
on p.product_id = d.product_id

set d.name = p.name
where  p.name <> '' and (p.date_added BETWEEN (now() - interval 15 minute) AND
 (now() + interval 1 minute) or p.date_modified BETWEEN (now() - interval 15 minute) AND (now() + interval 1 minute)
 )