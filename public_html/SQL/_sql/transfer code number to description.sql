-- 1) import
-- 2) set d.code = p.code
-- 3) drop primary key for product,
--    set p.product_id = d.product_id 
--    restore primary key to product


-- ** ================================ ** --
-- 2)
UPDATE oc_product_description as d
-- inner JOIN oc_product_to_category as c ON
-- p.product_id = c.product_id 


inner JOIN u0193535_oc.oc_product p
ON d.product_id = p.product_id


set d.code = p.code -- , d.date_modified = now()

-- where p.product_id = d.product_id

;

-- 3)
-- 3.1
ALTER TABLE `u0193535_oc`.`oc_product` 
CHANGE COLUMN `product_id` `product_id` INT(11) NOT NULL ,
DROP PRIMARY KEY;

-- 3.2
UPDATE u0193535_oc.oc_product p
inner JOIN u0193535_oc.oc_product_description as d
ON d.code = p.code

set 
  p.product_id = d.product_id


-- , p.date_modified = now()

;
-- 3.3
ALTER TABLE `u0193535_oc`.`oc_product` 
CHANGE COLUMN `product_id` `product_id` INT(11) NOT NULL AUTO_INCREMENT ,
ADD PRIMARY KEY (`product_id`);



-- UPDATE my_table
-- SET col_Z = IF(col_A = 4 OR col_B = 4, 4, NULL)
-- WHERE id = "001"

