SELECT p.product_id, p.code, p.price, p.model, d.name, d.description, d.meta_title, d.meta_keyword, d.tag  FROM u0193535_oc.oc_product  p
-- where product_id = 51776
left join u0193535_oc.oc_product_description  d
ON p.product_id = d.product_id
where p.product_id = 3958
order by code desc, description asc



/*
SELECT p.product_id, d.product_id as dpid, p.code, d.code as dcode, p.price, p.model, d.name, d.description, d.meta_title, d.meta_keyword, d.tag  FROM u0193535_oc.oc_product  p
-- where product_id = 51776
left join u0193535_oc.oc_product_description  d
ON p.product_id = d.product_id
-- order by p.code desc, description asc

-- ALTER TABLE `u0193535_oc`.`oc_product_description` 
-- ADD COLUMN `code` INT(15) NULL AFTER `product_id`;


*/