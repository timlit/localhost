-- http://stackoverflow.com/questions/4685173/delete-all-duplicate-rows-except-for-one-in-mysql
-- DELETE n1 FROM names n1, names n2 WHERE n1.id > n2.id AND n1.name = n2.name -- keep the row with the lowest id value
-- DELETE n1 FROM names n1, names n2 WHERE n1.id < n2.id AND n1.name = n2.name -- keep the row with the highest id value

-- Although the question is about DELETE, please be advised that using INSERT and DISTINCT is much faster. 
-- For a database with 8 million rows, the below query took 13 minutes, while using DELETE, it took more than 2 hours and yet didn't complete.

-- INSERT INTO tempTableName(cellId,attributeId,entityRowId,value)
--     SELECT DISTINCT cellId,attributeId,entityRowId,value
--    FROM tableName;


-- SELECT code, COUNT(*) c FROM oc_product GROUP BY code HAVING c > 1;
SELECT p.product_id, p.model, p.name, p.code 
FROM oc_product p
   INNER JOIN (SELECT code
               FROM   oc_product
               GROUP  BY code
               HAVING COUNT(code) > 1) dup
           ON p.code = dup.code
           
           order by code desc

           ;
