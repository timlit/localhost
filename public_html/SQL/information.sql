SELECT distinct * FROM u0193535_oc.oc_information_description id

left join u0193535_oc.oc_url_alias a
-- on a.query like concat("%", information_id, "%")

on a.query = concat ("information_id=", information_id)

where query is not null

/*

SELECT * FROM u0193535_oc.oc_information_description

where title like '%Добрая Аптека - %'
;	

*/
