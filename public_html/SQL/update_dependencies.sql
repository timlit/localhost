
set @date = date_format(now(), '%Y-%m-%d');
set @date = '2017-04-17';

-- insert into u0193535_oc.oc_product_description (product_id, code, language_id, name )


select distinct product_id, code, 1, name from u0193535_oc.oc_product 


-- date_added = '2016-09-27 14:21:32'
-- 
WHERE date_added BETWEEN concat( @date, ' 00:00:00' ) AND concat( @date, ' 23:59:59' )
-- WHERE date_added BETWEEN (now() - interval 15 minute) AND (now() + interval 1 minute)

;

/*
where DATE >= '2010-07-20' AND DATE < '2010-07-24'

WHERE columname BETWEEN '2012-12-25 00:00:00' AND '2012-12-25 23:59:59'

WHERE columname >='2012-12-25 00:00:00'
AND columname <'2012-12-26 00:00:00'


*/

