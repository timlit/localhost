CREATE VIEW `new_view` AS


SELECT *, drugs.DRUG_NAME_manufacturer as name_new

, 

locate(" N" , @name_new ) 
>0
/*
if 
(
locate(" N" , drugs.DRUG_NAME_manufacturer ) 
>0, 1, 0
)*/
as countN


,


   
case /* density */
when locate(binary "ЕД/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "ЕД/мл "
when locate(binary "МЕ/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "МЕ/мл "
when locate(binary "ЕД " , drugs.DRUG_NAME_manufacturer ) >0  then "ЕД/мл "
when locate(binary "МЕ " , drugs.DRUG_NAME_manufacturer ) >0 then "МЕ/мл "
when locate("ед/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "ед/мл "
when locate("мг/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "мг/мл "

else ' '

end
   
   as density
,

 
case /* density items*/
when case /* density */
when locate(binary "ЕД/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "ЕД/мл "
when locate(binary "МЕ/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "МЕ/мл "
when locate(binary "ЕД " , drugs.DRUG_NAME_manufacturer ) >0  then "ЕД/мл "
when locate(binary "МЕ " , drugs.DRUG_NAME_manufacturer ) >0 then "МЕ/мл "
when locate("ед/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "ед/мл "
when locate("мг/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "мг/мл "

else ' '

end <> ' '
 then 

SUBSTRING_INDEX  (
  SUBSTRING_INDEX (
drugs.DRUG_NAME_manufacturer,

case /* density */
when locate(binary "ЕД/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "ЕД/мл "
when locate(binary "МЕ/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "МЕ/мл "
when locate(binary "ЕД " , drugs.DRUG_NAME_manufacturer ) >0  then "ЕД/мл "
when locate(binary "МЕ " , drugs.DRUG_NAME_manufacturer ) >0 then "МЕ/мл "
when locate("ед/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "ед/мл "
when locate("мг/мл " , drugs.DRUG_NAME_manufacturer ) >0 then "мг/мл "

else ' '

end , 1) 
, ' ', -1 )

else ' '
end
as density_items


 FROM oc_spravka_drugs drugs
 
 