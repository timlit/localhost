/*
UPDATE `u0193535_oc`.`oc_product` p 
left join u0193535_oc.oc_product_description d
on p.product_id = d.product_id

SET 
-- p.date_available = p.date_added
-- p.date_modified = p.date_added
-- STATUS = 1
-- quantity = 0
-- 
price = 8000 
-- p.code = 230000357, d.code   = 230000357, quantity = 1
-- manufacturer_id = 2571

WHERE 
-- p.date_available is null and p.date_modified is  null
-- p.date_modified is  null
-- p.product_id = 60466 or p.product_id = 42474 -- 43358 -- 36816

p.product_id = 61675
-- p.model = 200087883002
-- p.product_id >=64389 and p.product_id <= 64391  -- 20000965
-- p.product_id = 51664 -- 64226 -- 62212 -- 64073
-- d.name like '%залтрап%' and p.product_id <>36379 and p.product_id <>36378

-- d.name like '%Герцептин%440%'
-- d.name like '%Герцептин, лиоф%' and image is null
 

-- quantity = 0
-- d.name like '%даклинза%'
-- d.name like '%селлсепт%'
-- d.name like '%Клексан%' and code = ''  or char_length(d.description) is null -- and char_length(d.description) = 0 and image = "" 
-- d.name like '%Инстенон, р-р д/инъ 2мл №5%' -- селлсепт Активтекс Инстенон, р-р д/инъ 2мл №5


-- where p.manufacturer_id = 1161
-- and price = 0 and image = '' 
-- and tag = '' and meta_description = '' and meta_h1 = '' and meta_keyword = '' and meta_title = ''
;
*/

/*
delete from oc_product 
where product_id >= 64485 and product_id <= 64487
;

 delete from oc_product_description 
where product_id >= 64485 and product_id <= 64487
;
*/

-- (CASE WHEN cp.path_id = c.parent_id THEN 1 ELSE 0 END ) as switch
SELECT distinct p.product_id, concat(cd.name, " (", p2c.category_id, ")") as cat, p.code, p.image, md.name as manufacturer,
p.name as pname, d.meta_h1, d.meta_title, d.meta_keyword, char_length(d.description) as countdescr,

/**/
(CASE 
WHEN 
char_length(d.description) is null 
AND quantity <= 0 
AND viewed = 0

THEN 1 
ELSE 0 END
) as toRemove,

p.viewed, p.status, p.quantity, p.price, p.minimum, d.product_id as dpid, md.manufacturer_id as mid, p.manufacturer_id
, d.name as dname, p.date_added, p.date_modified, p.date_available FROM u0193535_oc.oc_product p
-- where product_id > 64388

-- where product_id > 64391 and product_id < 64437
-- where name like '%МЫЛО NESTI КАРОЛИНА И ЭДУАРДО%'

-- where product_id = 51764
left join u0193535_oc.oc_product_description d
on p.product_id = d.product_id

left join u0193535_oc.oc_manufacturer_description md
on p.manufacturer_id = md.manufacturer_id

left join u0193535_oc.oc_product_to_category p2c
on p.product_id = p2c.product_id

left join u0193535_oc.oc_category_description cd
on p2c.category_id = cd.category_id

-- where quantity < 1
-- where d.name like '%Ампициллин-АКОС, пор. д/инъ в/м 1г фл №1%'



-- where d.name like '%Бараклюд%'
-- where d.name like '%vivo%'
-- where d.name like '%Амоксиклав%'
-- where d.name like '%Детралекс%'
-- where p.product_id = 61387 -- 64190 -- 62212
-- where d.name like '%Герцептин%440%№1%'
-- where d.name like '%Герцептин, лиоф%'
-- where d.name like '%квинакс%'

-- where image is null and d.description = '' -- or p.code = '' and image is null and viewed = 0
--  where d.name like '%Клексан%'
-- where d.name like '%Салофальк%30%'

-- where MONTH(date_available) = 5
-- where status = 0
-- where p.date_available is null -- and p.date_modified is null
-- where p.date_available = p.date_modified or p.date_available = p.date_added


-- where p.model = 200087883002
-- where d.name like '%залтрап%' -- AND p.image is not null
-- where d.name like '%Solbianсa%' or d.name like '%SOLBIANCA%'

-- where d.description <> ''
-- where d.name like '%Инстенон%' or p.name like '%Инстенон%'
/*where d.name like '%Элоксатин%' --
or  d.name like '%Гамунекс%' 
or  d.name like '%Тизабри%' -- 
or  d.name like '%Вотриент%' --
or  d.name like '%Мекинист%' -- 
or  d.name like '%Зинфоро%' --
or  d.name like '%Опдиво%' --
or  d.name like '%Дипептивен%' 
or  d.name like '%РотаТек%' --
or  d.name like '%Сандостатин Лар%' 
-- or  d.name like '%%' 
 
 or p.code =    20003419
 */   
 
 
-- where d.name like '%Физиогель Плюс Шампунь для волос 150мл%'
-- where d.name like '%Даклинза%'
-- where d.name like '%Метотрексат-Эбеве, р-р д/инъ 10мг/мл 1мл №10%'
-- where d.name = '' or p.name = ''
-- where p.name <> '' and p.manufacturer_id = 0
-- where p.code = 

where 
concat(

-- 
d.meta_title
/*
d.description -- 4296 row(s) returned
,d.meta_description -- 4153 row(s) returned
,d.meta_h1 -- 2027 row(s) returned
,d.meta_keyword -- 2750 row(s) returned
,d.meta_title -- 5867 row(s) returned

*/
)
<> ''
and status = 1
-- and quantity > 0


group by p.product_id
order by p.product_id desc
;

-- 66335

/*d.name like 'Абилифай 30 мг № 28%'
or d.name like '%Авастин 100 мг%'
or d.name like '%Авастин 400 мг%'
or d.name like '%Аллокин_Альфа №3%'
or d.name like '%Аллокин_Альфа №1%'
or d.name like '%Бараклюд 1  мг № 30%'
or d.name like '%Бараклюд 0,5 мг № 30%'
or d.name like '%Викейра_Пак%'
or d.name like '%Гонал 33 мкг/0,75 мл%'
or d.name like '%Гонал 22 мкг/0,5 мл%'
or d.name like '%Гонал 66 мкг/1,5 мл%'
or d.name like '%Дьюралан 60 мг/3 мл%'
or d.name like '%Менопур%'
or d.name like '%Октагам 50 мг/мл 100 мл%'
or d.name like '%Пурегон 300 МЕ%'
or d.name like '%Пурегон 600 МЕ%'
or d.name like '%Эксджива%'
or d.name like '%Пурегон 900 МЕ%'

*/


/*
update oc_product
set image = 'catalog/i/lf/ba/b11bc8ea569667eafb698847e3465e32.jpg'
where name like '%Даклинза%' and (image is null or image = '')

*/