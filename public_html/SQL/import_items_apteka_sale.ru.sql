-- truncate table u0193535_oc.import;
-- item, 
-- SELECT p.product_id, d.code, p.model as pmodel, d.bar as dbar, manufacturer_id, p.quantity as pq, d.quantity as dquantity, p.price, d.price as newprice, date_modified FROM u0193535_oc.oc_product p
SELECT p.product_id, d.code, p.model as pmodel, d.image, d.model as dmodel, p.quantity as pq, d.quantity as dquantity, p.price, d.price as newprice, d.date_modified 
FROM u0193535_oc.oc_product p

LEFT JOIN u0193535_oc.import d
ON p.sku = d.model

-- where 

-- p.price <> d.price  -- and d.price < 200

-- and (d.price < 200 and d.price > 100)
-- or p.quantity <> d.quantity
-- where p.price = d.price;
-- d.code = 104172




-- SELECT field,CONVERT(SUBSTRING_INDEX(field,'-',-1),UNSIGNED INTEGER) AS num
-- FROM table
-- ORDER BY num;

