/*
UPDATE  u0193535_oc.oc_product_description 
SET description = replace(description,'font-size: 11px;', '')

-- WHERE description like '%font-size%'
;
*/
SELECT p.product_id, quantity, p.code, p.name, date_available, date_added, expires, description, quantity, image FROM u0193535_oc.oc_product p
left join u0193535_oc.oc_product_description d
on p.product_id = d.product_id


-- WHERE description like '%<p>%' or description like '%</p>%' or description like '%&lt;p&gt;%' or description like '%&lt;/p&gt;%'
WHERE description like '%font-size%'
