CREATE TABLE `import` (
	`product_id` INT(11) NOT NULL AUTO_INCREMENT,
	`model` VARCHAR(64) NOT NULL,
	`sku` VARCHAR(64) NOT NULL,
	`upc` VARCHAR(12) NOT NULL,
	`ean` VARCHAR(14) NOT NULL,
	`jan` VARCHAR(13) NOT NULL,
	`isbn` VARCHAR(17) NOT NULL,
	`mpn` VARCHAR(64) NOT NULL,
	`location` VARCHAR(128) NOT NULL,
	`quantity` INT(4) NOT NULL DEFAULT '0',
	`stock_status_id` INT(11) NOT NULL,
	`image` VARCHAR(255) NULL DEFAULT NULL,
	`manufacturer_id` INT(11) NOT NULL,
	`shipping` TINYINT(1) NOT NULL DEFAULT '1',
	`price` DECIMAL(15,4) NOT NULL DEFAULT '0.0000',
	`points` INT(8) NOT NULL DEFAULT '0',
	`tax_class_id` INT(11) NOT NULL,
	`date_available` DATE NOT NULL DEFAULT '0000-00-00',
	`weight` DECIMAL(15,2) NOT NULL DEFAULT '0.00',
	`weight_class_id` INT(11) NOT NULL DEFAULT '0',
	`length` DECIMAL(15,2) NOT NULL DEFAULT '0.00',
	`width` DECIMAL(15,2) NOT NULL DEFAULT '0.00',
	`height` DECIMAL(15,2) NOT NULL DEFAULT '0.00',
	`length_class_id` INT(11) NOT NULL DEFAULT '0',
	`subtract` TINYINT(1) NOT NULL DEFAULT '1',
	`minimum` INT(11) NOT NULL DEFAULT '1',
	`sort_order` INT(11) NOT NULL DEFAULT '0',
	`status` TINYINT(1) NOT NULL DEFAULT '0',
	`viewed` INT(5) NOT NULL DEFAULT '0',
	`date_added` DATETIME NOT NULL,
	`date_modified` DATETIME NOT NULL,
	PRIMARY KEY (`product_id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
AUTO_INCREMENT=64389
;
