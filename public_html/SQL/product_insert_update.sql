-- set code primary key
ALTER TABLE `u0193535_oc`.`oc_product` 
CHANGE COLUMN `product_id` `product_id` INT(11) NOT NULL ,
CHANGE COLUMN `code` `code` INT(11) NOT NULL AUTO_INCREMENT ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`code`);

-- update products
INSERT INTO u0193535_oc.oc_product
(model, code, name, quantity, price, status, sort_order, date_available, date_added)
VALUES
('123456788', '123456788', 'wonderpill AA', '5', '85', 1, 100, STR_TO_DATE('14.03.2017', '%d.%m.%Y'), CURRENT_TIMESTAMP()),
('123456789', '123456789', 'wonderpill BB', '2', '14', 1, 100, STR_TO_DATE('14.03.2017', '%d.%m.%Y'), CURRENT_TIMESTAMP())
 
ON DUPLICATE KEY UPDATE
  name = VALUES(name)
, model = VALUES(model)
, quantity = VALUES(quantity)
, price = VALUES(price)
, date_available = VALUES(date_available)
, status = (1)
, sort_order = 100
, date_modified = (CURRENT_TIMESTAMP ())
;


UPDATE u0193535_oc.oc_product p
inner JOIN u0193535_oc.oc_product_description as d
ON d.code = p.code

set 
  p.product_id = d.product_id


-- , p.date_modified = now()

;

-- set product_id as primary key
ALTER TABLE `u0193535_oc`.`oc_product` 
CHANGE COLUMN `product_id` `product_id` INT(11) NOT NULL AUTO_INCREMENT ,
CHANGE COLUMN `code` `code` INT(11) NOT NULL ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`product_id`)
;



-- p.manufacturer_id
--
-- p.viewed
-- STR_TO_DATE('', '%d.%m.%Y')

/*
INSERT INTO `users_relationship` 
  (`profile_1_id`, `profile_2_id`
  ,`request_date`,`auth_token`,`status`, `updated`, `deleted`)
VALUES
  (2, 5, '2012-09-05 10:13:06', 'SQLTEST', 0, '2012-09-05 10:13:06', 0),
  (5, 2, '2012-09-05 10:13:06', 'SQLTEST', 0, '2012-09-05 10:13:06', 0) 
ON DUPLICATE KEY UPDATE
  request_date = VALUES(request_date)          -- what you want to happen
, auth_token = VALUES(auth_token)    -- when rows exist

*/