SELECT distinct p.product_id, pb.product_id as pbid, p.code, pb.code as pbcode,p.model, pb.model as pbmodel, p.image, pb.image as pbimage, d.description, pbd.description FROM u0193535_oc.oc_product p

left join u0193535_oc.oc_product_backup pb
-- on p.model = pb.model -- and  (pb.image <> '' and p.image = '')
-- 
on p.code = pb.code
-- on p.product_id = pb.product_id

left join u0193535_oc.oc_product_description d
on p.product_id = d.product_id

left join u0193535_oc.oc_product_description pbd
on pb.product_id = pbd.product_id
-- 
/*
where p.code <> '' 
and  (-- pb.image <> '' and p.image = ''
-- or 
d.description = '' and pbd.description <> ''
or d.meta_h1 = '' and pbd.meta_h1 <> ''

)
*/

;
