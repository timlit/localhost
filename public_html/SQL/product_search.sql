SELECT 
p.product_id,  p.code, p.price, p.image,
d.name as dname, p.name,  char_length(d.description) as countdescr, p.viewed, p.quantity, p.status, p.date_added, p.date_modified

 FROM u0193535_oc.oc_product p
inner join u0193535_oc.oc_product_description d
on p.product_id = d.product_id
-- 
where 
-- d.name like '%герцептин, лиоф%' -- or 
-- 
d.name like '%вигантол%' -- or 
-- p.code = '' and viewed > 100 and price > 2000 -- char_length(d.description) > 100

-- viewed = 0 and price > 3000 and ( date_added < (now()- INTERVAL 75 day)  ) -- and date_added > '2017-02-20 11:05:17' )

-- and image is null 
-- and char_length(d.description) is null and viewed = 0

-- quantity = 0 and viewed = 0 and p.code = '' 
-- and char_length(d.description) is null and image is null
/*
 d.name like '%Абаджио%'
 or d.name like '%Абилифай%'
 or d.name like '%Абраксан%'
 or d.name like '%Авастин%'
 or d.name like '%Акласта%'
 or d.name like '%Алимта%'
 or d.name like '%Аллокин_Альфа%'
 or d.name like '%Альбумин%'
 or d.name like '%Афинитор%'
 or d.name like '%Ацеллбия%'
 or d.name like '%Бараклюд%'
 or d.name like '%Бейодайм%'
 or d.name like '%Вайдаза%'
 or d.name like '%Вектибикс%'
 or d.name like '%Велкейд%'
 or d.name like '%Викейра_Пак%'
 or d.name like '%Виктоза%'
 or d.name like '%Вотриент%'
 or d.name like '%Герцептин%'
 or d.name like '%Гиотриф%'
 or d.name like '%Гливек%'
 or d.name like '%Гонал%'
 or d.name like '%Даклинза%'
 or d.name like '%Джевтана%'
 or d.name like '%Диферелин%'
 or d.name like '%Долгит%'
 or d.name like '%Доцетаксел%'
 or d.name like '%Дьюралан%'
 or d.name like '%Залтрап%'
 or d.name like '%Зелбораф%'
 or d.name like '%Зивокс%'
 or d.name like '%Зинфоро%'
 or d.name like '%Зитига%'
 or d.name like '%Зовиракс%'
 or d.name like '%Золадекс%'
 or d.name like '%Золинза%'
 or d.name like '%Зомета%'
 or d.name like '%Иматини%'
 or d.name like '%Ингавирин%'
 or d.name like '%Инлита%'
 or d.name like '%Иресса%'
 or d.name like '%Йонделис%'
 or d.name like '%Кагоцел%'
 or d.name like '%Кадсила%'
 or d.name like '%Капрелса%'
 or d.name like '%Касодекс%'
 or d.name like '%Келикс%'
 or d.name like '%Клексан%'
 or d.name like '%Копаксон%'
 or d.name like '%Кселода%'
 or d.name like '%Мабтера%'
 or d.name like '%Менопур%'
 or d.name like '%Нексавар%'
 or d.name like '%Октагам%'
 or d.name like '%Омнипак%'
 or d.name like '%Онкотрон%'
 or d.name like '%Остенил%'
 or d.name like '%Полиорикс%'
 or d.name like '%Пролиа%'
 or d.name like '%Пурегон%'
 or d.name like '%Ревлимид%'
 or d.name like '%Регогафениб%'
 or d.name like '%Ремикейд%'
 or d.name like '%Совриад%'
 or d.name like '%Стиварга%'
 or d.name like '%Сунвепра%'
 or d.name like '%Таксол%'
 or d.name like '%Таксотер%'
 or d.name like '%Тарцева%'
 or d.name like '%Тасигна%'
 or d.name like '%Тафинлар%'
 or d.name like '%Траумель%'
 or d.name like '%Трувада%'
 or d.name like '%Ферматрон%'
 or d.name like '%Фраксипарин%'
 or d.name like '%Хаврикс%'
 or d.name like '%Халавен%'
 or d.name like '%Хумира%'
 or d.name like '%Эйлеа%'
 or d.name like '%Эксджива%'
 or d.name like '%Элоксатин%'
 or d.name like '%Гамунекс%'
 or d.name like '%Тизабри%'
 or d.name like '%Вотриент%'
 or d.name like '%Мекинист%'
 or d.name like '%Зинфоро%'
 or d.name like '%Опдиво%'
 or d.name like '%Дипептивен%'
 or d.name like '%РотаТек%'
 or d.name like '%Сандостатин Лар%'
;

*/