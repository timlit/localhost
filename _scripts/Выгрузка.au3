;~ #include <FTP.au3>;
;~ #include <FTPEx.au3>
#include <Constants.au3>
#include <GUIConstantsEx.au3>




#include <forceutf8.au3>
#include <Date.au3>
#include <SmtpMailer.au3>
#include <Inet.au3>
#include <MsgBoxConstants.au3>

Local $s_SmtpServer = "smtp.yandex.ru"
Local $s_FromAddress = "aptekasale2017@yandex.ru"
Local $s_FromName = "Maroseika"
Local $s_Username = "aptekasale2017@yandex.ru"
Local $s_Password = "aptekasale"

Local $b_SSL = True
Local $s_Port = "465"
Local $s_ToAddress = "aptekasale2017@yandex.ru"



if StringInStr (@ScriptName, "24") Then
Local $file = "Лист1.xlsx"
Local $site = "pharmacy24h.ru"
Local $s_Subject = "Import pharmacy24h.ru OK " & _Now()
Local $s_Body = "Import (" & $file & ") pharmacy24h.ru OK " & _Now()
$ftp_server = '31.31.196.112'
$ftp_user_name = 'u0200938_admin'
$ftp_user_pass = 'vdHCGSjt'
$path = "www/pharmacy24h.ru/"
$path = ""
Else;
Local $file = "Лист1.xlsx"
Local $site = "apteka-sale.ru"
Local $s_Subject = "Import apteka-sale.ru OK " & _Now()
Local $s_Body = "Import  (" & $file & ") apteka-sale.ru OK " & _Now()
$ftp_server = '31.31.196.112'
$ftp_user_name = 'u0193535_bulat'
$ftp_user_pass = 'iizdVJ0J'
$path = "www/new.apteka-sale.ru/"
$path = ""
EndIf


Local $hMainGUI = GUICreate("Выгрузка на " & $site, 350, 100)
GUICtrlCreateLabel("Выгрузка на сайт" & $site, 30, 10)
;~ Local $iOKButton = GUICtrlCreateButton("OK", 70, 50, 60)
Local $idProgressbar1 = GUICtrlCreateProgress(10, 10, 330, 20)

Local $hDummyGUI = GUICreate("Dummy window for testing", 200, 100)

GUISwitch($hMainGUI)
GUISetState(@SW_SHOW)

Local $aMsg = 0

; Exute while in GUI mode
While 1
if Not FileExists ( $file ) Then
MsgBox ( 0 , "Файл отсутствует","Файл " & $file & " не существует. Разместите файл выгрузки рядом с файлом программы " & @ScriptName ) ; , FileExists ( $file )
Exit
Else;

EndIf

if _internetok() = 0 Then
	MsgBox(0, "No Internet", "Проверьте доступ в интернет.")
	Exit;
;~ Else;
;~ 	MsgBox(0,"OK","OK")
EndIf

;~ Exit
GUICtrlSetData($idProgressbar1, GUICtrlRead($idProgressbar1) + 30)

$Open = _FTPOpen('MyFTP' & _Now() & Random(65, 122, 1) ) ;
$Conn = _FTPConnect($Open, $ftp_server, $ftp_user_name, $ftp_user_pass)
$Ftpp = _FtpPutFile($Conn, $file, $path & $file)
$Ftpc = _FTPClose($Open)

if $Ftpp = 0 Then
	MsgBox 'Ошибка передачи файла';
	Exit
Else
GUICtrlSetData($idProgressbar1, GUICtrlRead($idProgressbar1) + 30)
Sleep(3  *1000);
_SMTP_SendEmail($s_SmtpServer, $s_Username, $s_Password, $s_FromName, $s_FromAddress, $s_ToAddress,$s_Subject,$s_Body,"","","", "Normal",$s_Port, $b_SSL)


EndIf

;  $s_Subject = "", $s_Body = "", $s_AttachFiles = "", $s_CcAddress = "", $s_BccAddress = "",
;~ $s_Importance = "Normal", $i_IPPort = 25, $b_SSL = False, $b_IsHTMLBody = False, $i_DSNOptions = $g__cdoDSNDefault, $sEMLPath_SaveBefore = '', $sEMLPath_SaveAfter = ''
GUICtrlSetData($idProgressbar1, GUICtrlRead($idProgressbar1) + 30)
FileDelete ($file);

MsgBox(0,"", "Файл выгрузки отправлен! " & @CRLF & "Спасибо!", 1) ; @ScriptDir & @CRLF & @WorkingDir

ExitLoop


WEnd
Exit



;; FUNCTIONS ;;
Func _internetok()

Global $site = "http://www.ntp.org" ; or "http://www.google.com"
Global $up = InetGetSize($site, 1) ; "1" forces page load from the internet
If @error = 0 Then
	Return 1;
;~  MsgBox(0, "Internet", "Up ! (" & $site & " returned a file size of " & $up & " bytes)")
Else
	Return 0;
;~  MsgBox(0, "Internet", "Down ! (" & $site & " returned an error)")
;~  MsgBox(0, "No Internet", "No internet? ")
;~  Exit;
EndIf

EndFunc

;===============================================================================
;
; Function Name:    _FTPOpen()
; Description:      Opens an FTP session.
; Parameter(s):     $s_Agent      	- Random name. ( like "myftp" )
;                   $l_AccessType 	- I dont got a clue what this does.
;                   $s_ProxyName  	- ProxyName.
;                   $s_ProxyBypass	- ProxyByPasses's.
;                   $l_Flags       	- Special flags.
; Requirement(s):   DllCall, wininet.dll
; Return Value(s):  On Success - Returns an indentifier.
;                   On Failure - 0  and sets @ERROR
; Author(s):        Wouter van Kesteren.
;
;===============================================================================

Func _FTPOpen($s_Agent, $l_AccessType = 1, $s_ProxyName = '', $s_ProxyBypass = '', $l_Flags = 0)

	Local $ai_InternetOpen = DllCall('wininet.dll', 'long', 'InternetOpen', 'str', $s_Agent, 'long', $l_AccessType, 'str', $s_ProxyName, 'str', $s_ProxyBypass, 'long', $l_Flags)
	If @error OR $ai_InternetOpen[0] = 0 Then
		SetError(-1)
		Return 0
	EndIf

	Return $ai_InternetOpen[0]

EndFunc ;==> _FTPOpen()

;===============================================================================
;
; Function Name:    _FTPConnect()
; Description:      Connects to an FTP server.
; Parameter(s):     $l_InternetSession	- The Long from _FTPOpen()
;                   $s_ServerName 		- Server name/ip.
;                   $s_Username  		- Username.
;                   $s_Password			- Password.
;                   $i_ServerPort  		- Server port ( 0 is default (21) )
;					$l_Service			- I dont got a clue what this does.
;					$l_Flags			- Special flags.
;					$l_Context			- I dont got a clue what this does.
; Requirement(s):   DllCall, wininet.dll
; Return Value(s):  On Success - Returns an indentifier.
;                   On Failure - 0  and sets @ERROR
; Author(s):        Wouter van Kesteren
;
;===============================================================================

Func _FTPConnect($l_InternetSession, $s_ServerName, $s_Username, $s_Password, $i_ServerPort = 0, $l_Service = 1, $l_Flags = 0, $l_Context = 0)

	Local $ai_InternetConnect = DllCall('wininet.dll', 'long', 'InternetConnect', 'long', $l_InternetSession, 'str', $s_ServerName, 'int', $i_ServerPort, 'str', $s_Username, 'str', $s_Password, 'long', $l_Service, 'long', $l_Flags, 'long', $l_Context)
	If @error OR $ai_InternetConnect[0] = 0 Then
		SetError(-1)
		Return 0
	EndIf

	Return $ai_InternetConnect[0]

EndFunc ;==> _FTPConnect()

;===============================================================================
;
; Function Name:    _FTPPutFile()
; Description:      Puts an file on an FTP server.
; Parameter(s):     $l_FTPSession	- The Long from _FTPConnect()
;                   $s_LocalFile 	- The local file.
;                   $s_RemoteFile  	- The remote Location for the file.
;                   $l_Flags		- Special flags.
;                   $l_Context  	- I dont got a clue what this does.
; Requirement(s):   DllCall, wininet.dll
; Return Value(s):  On Success - 1
;                   On Failure - 0
; Author(s):        Wouter van Kesteren
;
;===============================================================================

Func _FTPPutFile($l_FTPSession, $s_LocalFile, $s_RemoteFile, $l_Flags = 0, $l_Context = 0)

	Local $ai_FTPPutFile = DllCall('wininet.dll', 'int', 'FtpPutFile', 'long', $l_FTPSession, 'str', $s_LocalFile, 'str', $s_RemoteFile, 'long', $l_Flags, 'long', $l_Context)
	If @error OR $ai_FTPPutFile[0] = 0 Then
		SetError(-1)
		Return 0
	EndIf

	Return $ai_FTPPutFile[0]

EndFunc ;==> _FTPPutFile()

;===============================================================================
;
; Function Name:    _FTPDelFile()
; Description:      Delete an file from an FTP server.
; Parameter(s):     $l_FTPSession	- The Long from _FTPConnect()
;                   $s_RemoteFile  	- The remote Location for the file.
; Requirement(s):   DllCall, wininet.dll
; Return Value(s):  On Success - 1
;                   On Failure - 0
; Author(s):        Wouter van Kesteren
;
;===============================================================================

Func _FTPDelFile($l_FTPSession, $s_RemoteFile)

	Local $ai_FTPPutFile = DllCall('wininet.dll', 'int', 'FtpDeleteFile', 'long', $l_FTPSession, 'str', $s_RemoteFile)
	If @error OR $ai_FTPPutFile[0] = 0 Then
		SetError(-1)
		Return 0
	EndIf

	Return $ai_FTPPutFile[0]

EndFunc ;==> _FTPDelFile()

;===============================================================================
;
; Function Name:    _FTPRenameFile()
; Description:      Renames an file on an FTP server.
; Parameter(s):     $l_FTPSession	- The Long from _FTPConnect()
;                   $s_Existing 	- The old file name.
;                   $s_New  		- The new file name.
; Requirement(s):   DllCall, wininet.dll
; Return Value(s):  On Success - 1
;                   On Failure - 0
; Author(s):        Wouter van Kesteren
;
;===============================================================================

Func _FTPRenameFile($l_FTPSession, $s_Existing, $s_New)

	Local $ai_FTPRenameFile = DllCall('wininet.dll', 'int', 'FtpRenameFile', 'long', $l_FTPSession, 'str', $s_Existing, 'str', $s_New)
	If @error OR $ai_FTPRenameFile[0] = 0 Then
		SetError(-1)
		Return 0
	EndIf

	Return $ai_FTPRenameFile[0]

EndFunc ;==> _FTPRenameFile()

;===============================================================================
;
; Function Name:    _FTPMakeDir()
; Description:      Makes an Directory on an FTP server.
; Parameter(s):     $l_FTPSession	- The Long from _FTPConnect()
;                   $s_Remote 		- The file name to be deleted.
; Requirement(s):   DllCall, wininet.dll
; Return Value(s):  On Success - 1
;                   On Failure - 0
; Author(s):        Wouter van Kesteren
;
;===============================================================================

Func _FTPMakeDir($l_FTPSession, $s_Remote)

	Local $ai_FTPMakeDir = DllCall('wininet.dll', 'int', 'FtpCreateDirectory', 'long', $l_FTPSession, 'str', $s_Remote)
	If @error OR $ai_FTPMakeDir[0] = 0 Then
		SetError(-1)
		Return 0
	EndIf

	Return $ai_FTPMakeDir[0]

EndFunc ;==> _FTPMakeDir()

;===============================================================================
;
; Function Name:    _FTPDelDir()
; Description:      Delete's an Directory on an FTP server.
; Parameter(s):     $l_FTPSession	- The Long from _FTPConnect()
;                   $s_Remote 		- The Directory to be deleted.
; Requirement(s):   DllCall, wininet.dll
; Return Value(s):  On Success - 1
;                   On Failure - 0
; Author(s):        Wouter van Kesteren
;
;===============================================================================

Func _FTPDelDir($l_FTPSession, $s_Remote)

	Local $ai_FTPDelDir = DllCall('wininet.dll', 'int', 'FtpRemoveDirectory', 'long', $l_FTPSession, 'str', $s_Remote)
	If @error OR $ai_FTPDelDir[0] = 0 Then
		SetError(-1)
		Return 0
	EndIf

	Return $ai_FTPDelDir[0]

EndFunc ;==> _FTPDelDir()

;===============================================================================
;
; Function Name:    _FTPClose()
; Description:      Closes the _FTPOpen session.
; Parameter(s):     $l_InternetSession	- The Long from _FTPOpen()
; Requirement(s):   DllCall, wininet.dll
; Return Value(s):  On Success - 1
;                   On Failure - 0
; Author(s):        Wouter van Kesteren
;
;===============================================================================

Func _FTPClose($l_InternetSession)

	Local $ai_InternetCloseHandle = DllCall('wininet.dll', 'int', 'InternetCloseHandle', 'long', $l_InternetSession)
	If @error OR $ai_InternetCloseHandle[0] = 0 Then
		SetError(-1)
		Return 0
	EndIf

	Return $ai_InternetCloseHandle[0]

EndFunc ;==> _FTPClose()