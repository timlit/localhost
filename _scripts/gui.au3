#include <Constants.au3>
#include <GUIConstantsEx.au3>

Local $hMainGUI = GUICreate("Hello World", 250, 100)
GUICtrlCreateLabel("Hello world! How are you?", 30, 10)
Local $iOKButton = GUICtrlCreateButton("OK", 70, 50, 60)
Local $idProgressbar1 = GUICtrlCreateProgress(10, 10, 230, 20)

Local $hDummyGUI = GUICreate("Dummy window for testing", 200, 100)

GUISwitch($hMainGUI)
GUISetState(@SW_SHOW)

Local $aMsg = 0
While 1
    $aMsg = GUIGetMsg(1)

    Select
        Case $aMsg[0] = $iOKButton
            ;MsgBox($MB_SYSTEMMODAL, "GUI Event", "You selected OK!")
			GUICtrlSetData($idProgressbar1, GUICtrlRead($idProgressbar1) + 30)


        Case $aMsg[0] = $GUI_EVENT_CLOSE And $aMsg[1] = $hMainGUI
            ;MsgBox($MB_SYSTEMMODAL, "GUI Event", "You selected CLOSE on the main GUI! Exiting...")
            ExitLoop
    EndSelect
WEnd


